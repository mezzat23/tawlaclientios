//
//  Storyboards.swift
//  SupportI
//
//  Created by Mohamed Abdu on 3/20/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import Foundation

public enum Storyboards: String {
    case main = "Main"
    case auth = "Auth"
    case other = "Other"
    case pop = "Popup"
    case resturant = "Resturant"
    case setting = "Setting"
    case reservation = "Reservation"
    case PickerViewHelper
    case book = "Book"
    
}
