//
//  Myview.swift
//  Timer
//
//  Created by Graphic on 4/31/1397 AP.
//  Copyright © 1397 Graphic. All rights reserved.
//

import UIKit
@IBDesignable
class Myview: UIView {

    @IBInspectable var firstcolor : UIColor = UIColor.clear{
        didSet {
            updateview()
        }
    }
    @IBInspectable var secondcolor : UIColor = UIColor.clear{
        didSet {
           updateview()
        }
    }
    override class var layerClass : AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    func updateview(){
        let layer = self.layer as! CAGradientLayer
        layer.colors = [firstcolor.cgColor , secondcolor.cgColor]
    }
    
}
