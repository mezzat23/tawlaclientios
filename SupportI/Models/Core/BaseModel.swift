//
//  User.swift
//  homeCheif
//
//  Created by Algazzar on 4/1/18.
//  Copyright © 2018 Atiaf. All rights reserved.
//

import CoreData

class BaseModel: Decodable {

    var success: Bool?
    var message: String?
    var errors: Errors?
    var error: Bool?
    var data: String?

}
class ErrorModel: Decodable {
    
    var success: Bool?
    var message: Errors?
    var errors: Errors?
    var error: Bool?
    var data: String?
    
}
