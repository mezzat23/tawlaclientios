// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let pageModel = try? newJSONDecoder().decode(PageModel.self, from: jsonData)

import Foundation

// MARK: - PageModel
struct PageModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: DataClass?
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let id: Int?
        let name, dataDescription: String?
        
        enum CodingKeys: String, CodingKey {
            case id, name
            case dataDescription = "description"
        }
    }

}
