// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let menuModel = try? newJSONDecoder().decode(MenuModel.self, from: jsonData)

import Foundation

// MARK: - MenuModel
struct MenuResModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: DataClass?
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let data: [Datum]?
        
    }
    
    // MARK: - Datum
    struct Datum: Codable {
        let id: Int?
        let price: String?
        let placeID: Int?
        let createdAt, updatedAt, name, details: String?
        let translations: [Translation]?
        
        enum CodingKeys: String, CodingKey {
            case id, price
            case placeID = "place_id"
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case name, details, translations
        }
    }
    
    // MARK: - Translation
    struct Translation: Codable {
        let id, menuItemID: Int?
        let name, details, locale: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case menuItemID = "menu_item_id"
            case name, details, locale
        }
    }

}
