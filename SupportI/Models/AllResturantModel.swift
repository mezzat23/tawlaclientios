// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let allResturantModel = try? newJSONDecoder().decode(AllResturantModel.self, from: jsonData)

import Foundation

// MARK: - AllResturantModel
struct AllResturantModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: [Datum]?
    
    
    // MARK: - Datum
    struct Datum: Codable {
        let id: Int?
        let name, address, dressType, priceNote: String?
        let paymentTypes, familtiesChildrenNote: String?
        let hasChildren: Bool?
        let area, city, country: String?
        let phones: [String]?
        let placeOwner, details, workingSchedule, rate: String?
        let latitude, longitude: String?
        let bannerLink: String?
        let image: String?
        let placeType: PlaceType?
        let offers: [Offer]?
        let placeFeatures, placeServiceType: [PlaceType]?
        let rates: [Rate]?
        let socialMedia: SocialMedia?
        let album, menuPics: [Album]?
        let menuItems: [MenuItem]?
        let menuNum: Int?
        let links: Links?
        let ratesNum, reservationsNum, placeTablesNum: Int?
        let placeTables: [PlaceTable]?
        let closed: Int?
        let closestartdate, closeenddate: String?
        let workingDays: [WorkingDay]?
        let createdAt: String?
        
        enum CodingKeys: String, CodingKey {
            case id, name, address
            case dressType = "dress_type"
            case priceNote = "price_note"
            case paymentTypes = "payment_types"
            case familtiesChildrenNote = "familties_children_note"
            case hasChildren = "has_children"
            case area, city, country, phones
            case placeOwner = "place_owner"
            case details
            case workingSchedule = "working_schedule"
            case rate, latitude, longitude
            case bannerLink = "banner_link"
            case image
            case placeType = "place_type"
            case offers
            case placeFeatures = "place_features"
            case placeServiceType = "place_service_type"
            case rates
            case socialMedia = "social_media"
            case album
            case menuPics = "menu_pics"
            case menuItems = "menu_items"
            case menuNum = "menu_num"
            case links
            case ratesNum = "rates_num"
            case reservationsNum = "reservations_num"
            case placeTablesNum = "place_tables_num"
            case placeTables = "place_tables"
            case closed, closestartdate, closeenddate, workingDays
            case createdAt = "created_at"
        }
    }
    
    // MARK: - Album
    struct Album: Codable {
        let mediaID: Int?
        let link: String?
        
        enum CodingKeys: String, CodingKey {
            case mediaID = "media_id"
            case link
        }
    }
    
    // MARK: - CreatedAtClass
    struct CreatedAtClass: Codable {
        let date: String?
        let timezoneType: Int?
        let timezone: String?
        
        enum CodingKeys: String, CodingKey {
            case date
            case timezoneType = "timezone_type"
            case timezone
        }
    }
    
    // MARK: - Links
    struct Links: Codable {
        let linksStatic: String?
        
        enum CodingKeys: String, CodingKey {
            case linksStatic = "_static"
        }
    }
    
    // MARK: - MenuItem
    struct MenuItem: Codable {
        let id: Int?
        let name, details, price: String?
        let image: String?
    }
    
    // MARK: - Offer
    struct Offer: Codable {
        let id: Int?
        let name, price, startDate, dueDate: String?
        let image: String?
        
        enum CodingKeys: String, CodingKey {
            case id, name, price
            case startDate = "start_date"
            case dueDate = "due_date"
            case image
        }
    }
    
    // MARK: - PlaceType
    struct PlaceType: Codable {
        let id: Int?
        let name: String?
        let image: String?
        let placeID: Int?
        
        enum CodingKeys: String, CodingKey {
            case id, name, image
            case placeID = "place_id"
        }
    }
    
    // MARK: - PlaceTable
    struct PlaceTable: Codable {
        let id, tableTypeID, placeID, number: Int?
        let capacityFrom, capacityTo, hasBarrier: Int?
        let position: String?
        let price, createdAt, updatedAt: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case tableTypeID = "table_type_id"
            case placeID = "place_id"
            case number
            case capacityFrom = "capacity_from"
            case capacityTo = "capacity_to"
            case hasBarrier = "has_barrier"
            case position, price
            case createdAt = "created_at"
            case updatedAt = "updated_at"
        }
    }
    
    // MARK: - Rate
    struct Rate: Codable {
        let id, serviceRate, foodRate, placeRate: Double?
        let rateComment: String?
        let user, place: PlaceType?
        let createdAt: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case serviceRate = "service_rate"
            case foodRate = "food_rate"
            case placeRate = "place_rate"
            case rateComment = "rate_comment"
            case user, place
            case createdAt = "created_at"
        }
    }
    
    // MARK: - SocialMedia
    struct SocialMedia: Codable {
        let twitter: String?
        let youtube, facebook: String?
        let instagram: String?
        let google: String?
    }
    
    // MARK: - WorkingDay
    struct WorkingDay: Codable {
        let id, placeID: Int?
        let day: String?
        let startAt, endAt, reservationMechanism: String?
        let reservationTime: Int?
        let createdAt: String?
        let updatedAt: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case placeID = "place_id"
            case day
            case startAt = "start_at"
            case endAt = "end_at"
            case reservationMechanism = "reservation_mechanism"
            case reservationTime = "reservation_time"
            case createdAt = "created_at"
            case updatedAt = "updated_at"
        }
    }
}
