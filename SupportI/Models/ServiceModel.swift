// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let serviceModel = try? newJSONDecoder().decode(ServiceModel.self, from: jsonData)

import Foundation

// MARK: - ServiceModel
struct ServiceModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: DataClass?
    
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let currentPage: Int?
        let data: [Datum]?
        let firstPageURL: String?
        let from, lastPage: Int?
        let lastPageURL, path: String?
        let perPage, to, total: Int?
        
        enum CodingKeys: String, CodingKey {
            case currentPage = "current_page"
            case data
            case firstPageURL = "first_page_url"
            case from
            case lastPage = "last_page"
            case lastPageURL = "last_page_url"
            case path
            case perPage = "per_page"
            case to, total
        }
    }
    
    // MARK: - Datum
    struct Datum: Codable {
        let id, placeTypeID: Int?
        let createdAt, updatedAt, name: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case placeTypeID = "place_type_id"
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case name
        }
    }

    
}
