// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let invoiceModel = try? newJSONDecoder().decode(InvoiceModel.self, from: jsonData)

import Foundation

// MARK: - InvoiceModel
struct InvoiceModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: DataClass?
    
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let resID: Int?
        let table: DataTable?
        let resNum, resDate, resStartTime, resEndTime: String?
        let resPayment, resTablePrice: String?
        let resGatheringType: ResGatheringType?
        let resTableNumber: Int?
        let products: [Product]?
        let productsSubPrice, productsTotalPrice, resSubTotalPrice, resCouponPrice, resVatPrice: Double?
        let resTotalPrice: Double?
        let qr: String?
        
        enum CodingKeys: String, CodingKey {
            case resID = "res_id"
            case table
            case resNum = "res_num"
            case resDate = "res_date"
            case resStartTime = "res_start_time"
            case resEndTime = "res_end_time"
            case resPayment = "res_payment"
            case resTablePrice = "res_table_price"
            case resGatheringType = "res_gathering_type"
            case resTableNumber = "res_table_number"
            case products
            case productsSubPrice = "products_sub_price"
            case productsTotalPrice = "products_total_price"
            case resSubTotalPrice = "res_sub_total_price"
            case resVatPrice = "res_vat_price"
            case resTotalPrice = "res_total_price"
            case resCouponPrice = "res_coupon_price"
            case qr
        }
    }
    
    // MARK: - Product
    struct Product: Codable {
        let id: Int?
        let price: String?
        let placeID, menuCatID: Int?
        let createdAt, updatedAt: String?
        let quantity: Int?
        let subTotal: String?
        let image: String?
        let name, details: String?
        let translations: [ProductTranslation]?
        
        enum CodingKeys: String, CodingKey {
            case id, price
            case placeID = "place_id"
            case menuCatID = "menu_cat_id"
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case quantity
            case subTotal = "sub_total"
            case image, name, details, translations
        }
    }
    
    // MARK: - ProductTranslation
    struct ProductTranslation: Codable {
        let id, menuItemID: Int?
        let name, details, locale: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case menuItemID = "menu_item_id"
            case name, details, locale
        }
    }
    
    // MARK: - ResGatheringType
    struct ResGatheringType: Codable {
        let id: Int?
        let createdAt, updatedAt, name: String?
        let pivot: Pivot?
        let translations: [ResGatheringTypeTranslation]?
        
        enum CodingKeys: String, CodingKey {
            case id
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case name, pivot, translations
        }
    }
    
    // MARK: - Pivot
    struct Pivot: Codable {
        let placeID, gatheringTypeID: Int?
        
        enum CodingKeys: String, CodingKey {
            case placeID = "place_id"
            case gatheringTypeID = "gathering_type_id"
        }
    }
    
    // MARK: - ResGatheringTypeTranslation
    struct ResGatheringTypeTranslation: Codable {
        let id, gatheringTypeID: Int?
        let name, locale: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case gatheringTypeID = "gathering_type_id"
            case name, locale
        }
    }
    
    // MARK: - DataTable
    struct DataTable: Codable {
        let id: Int?
        let customer: Customer?
        let place: GatheringType?
        let table: TableTable?
        let gatheringType: GatheringType?
        let status, statusMessage: String?
        let adultCount, childrenCount: Int?
        let notes, createdAt, date, startAt: String?
        let mechanism, customerName, customerMobile: String?
        let canSendQueueReservationNotification: Bool?
        let price: String?
        let rated: Int?
        
        enum CodingKeys: String, CodingKey {
            case id, customer, place, table
            case gatheringType = "gathering_type"
            case status
            case statusMessage = "status_message"
            case adultCount = "adult_count"
            case childrenCount = "children_count"
            case notes
            case createdAt = "created_at"
            case date
            case startAt = "start_at"
            case mechanism
            case customerName = "customer_name"
            case customerMobile = "customer_mobile"
            case canSendQueueReservationNotification = "can_send_queue_reservation_notification"
            case price, rated
        }
    }
    
    // MARK: - Customer
    struct Customer: Codable {
        let id: Int?
        let name, mobile, email, type: String?
        let verified: Int?
        let gender: String?
        let avatar: String?
    }
    
    // MARK: - GatheringType
    struct GatheringType: Codable {
        let id: Int?
        let name: String?
        let image: String?
        let paymentTypes: String?
        let barrierable: Bool?
        
        enum CodingKeys: String, CodingKey {
            case id, name, image
            case paymentTypes = "payment_types"
            case barrierable
        }
    }
    
    // MARK: - TableTable
    struct TableTable: Codable {
        let id, number, capacityFrom, capacityTo: Int?
        let hasBarrier: Int?
        let position: String?
        let tableType: GatheringType?
        let availability: Bool?
        
        enum CodingKeys: String, CodingKey {
            case id, number
            case capacityFrom = "capacity_from"
            case capacityTo = "capacity_to"
            case hasBarrier = "has_barrier"
            case position
            case tableType = "table_type"
            case availability
        }
    }

}
