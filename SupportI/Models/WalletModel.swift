// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let walletModel = try? newJSONDecoder().decode(WalletModel.self, from: jsonData)

import Foundation

// MARK: - WalletModel
struct WalletModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: DataClass?
    let wallet: String?
    
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let currentPage: Int?
        let data: [Datum]?
        let firstPageURL: String?
        let from, lastPage: Int?
        let lastPageURL, path: String?
        let perPage, to, total: Int?
        
        enum CodingKeys: String, CodingKey {
            case currentPage = "current_page"
            case data
            case firstPageURL = "first_page_url"
            case from
            case lastPage = "last_page"
            case lastPageURL = "last_page_url"
            case path
            case perPage = "per_page"
            case to, total
        }
    }
    
    // MARK: - Datum
    struct Datum: Codable {
        let id, userID: Int?
        let type, value, note, createdAt: String?
        let updatedAt, txt: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case userID = "user_id"
            case type, value, note
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case txt
        }
    }

    
}
