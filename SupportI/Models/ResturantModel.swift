// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let resturantModel = try? newJSONDecoder().decode(ResturantModel.self, from: jsonData)

import Foundation

struct ResturantsModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: [ResturantModel.DataClass]?
}
struct RatesModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: [AllResturantModel.Rate]?
    let data2: Data2?
    // MARK: - Data2
    struct Data2: Codable {
        let rateCount: Int?
        let allServiceRate, allFoodRate: Double?
        let allPlaceRate: Double?
        
        enum CodingKeys: String, CodingKey {
            case rateCount = "rate_count"
            case allServiceRate = "all_service_rate"
            case allFoodRate = "all_food_rate"
            case allPlaceRate = "all_place_rate"
        }
    }
}
// MARK: - ResturantModel
struct ResturantModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: AllResturantModel.Datum?
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let id: Int?
        let name, address, dressType, priceNote: String?
        let paymentTypes, familtiesChildrenNote: String?
        let hasChildren: Bool?
        let area, city, country: String?
        let phones: [String]?
        let placeOwner, details, workingSchedule, rate: String?
        let latitude, longitude, bannerLink: String?
        let image: String?
        let placeType: Place?
        let offers: [Offer]?
        let placeFeatures: [Place]?
        let rates: [Rate]?
        let socialMedia: SocialMedia?
        let album, menu: [Album]?
        let links: Links?
        let ratesNum, reservationsNum: Int?
        let placeTables: [PlaceTable]?
        
        enum CodingKeys: String, CodingKey {
            case id, name, address
            case dressType = "dress_type"
            case priceNote = "price_note"
            case paymentTypes = "payment_types"
            case familtiesChildrenNote = "familties_children_note"
            case hasChildren = "has_children"
            case area, city, country, phones
            case placeOwner = "place_owner"
            case details
            case workingSchedule = "working_schedule"
            case rate, latitude, longitude
            case bannerLink = "banner_link"
            case image
            case placeType = "place_type"
            case offers
            case placeFeatures = "place_features"
            case rates
            case socialMedia = "social_media"
            case album, menu, links
            case ratesNum = "rates_num"
            case reservationsNum = "reservations_num"
            case placeTables = "place_tables"
        }
    }
    
    // MARK: - Album
    struct Album: Codable {
        let mediaID: Int?
        let link: String?
        
        enum CodingKeys: String, CodingKey {
            case mediaID = "media_id"
            case link
        }
    }
    
    // MARK: - Links
    struct Links: Codable {
        let linksStatic: String?
        
        enum CodingKeys: String, CodingKey {
            case linksStatic = "_static"
        }
    }
    
    // MARK: - Place
    struct Place: Codable {
        let id: Int?
        let name: String?
        let image: String?
        let placeID: Int?
        
        enum CodingKeys: String, CodingKey {
            case id, name, image
            case placeID = "place_id"
        }
    }
    // MARK: - Offer
    struct Offer: Codable {
        let id: Int?
        let name: String?
        let image: String?
        let price: String?
        let dueDate: String?
        enum CodingKeys: String, CodingKey {
            case id, name, image, price
            case dueDate = "due_date"
        }
    }
    
    // MARK: - PlaceTable
    struct PlaceTable: Codable {
        let id, tableTypeID, placeID, number: Int?
        let capacityFrom, capacityTo, hasBarrier: Int?
        let position, createdAt, updatedAt: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case tableTypeID = "table_type_id"
            case placeID = "place_id"
            case number
            case capacityFrom = "capacity_from"
            case capacityTo = "capacity_to"
            case hasBarrier = "has_barrier"
            case position
            case createdAt = "created_at"
            case updatedAt = "updated_at"
        }
    }
    
    // MARK: - Rate
    struct Rate: Codable {
        let id, serviceRate, foodRate, placeRate: Int?
        let rateComment: String?
        let user: User?
        let place: Place?
        let createdAt: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case serviceRate = "service_rate"
            case foodRate = "food_rate"
            case placeRate = "place_rate"
            case rateComment = "rate_comment"
            case user, place
            case createdAt = "created_at"
        }
    }
    
    // MARK: - User
    struct User: Codable {
        let id: Int?
        let name: String?
        let image: String?
    }
    
    // MARK: - SocialMedia
    struct SocialMedia: Codable {
        let twitter: String?
        let youtube, facebook, instagram: String?
        let google: String?
    }

}
