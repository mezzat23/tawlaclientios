//
//  BookModel.swift
//  Tawla-client
//
//  Created by M.abdu on 7/7/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import Foundation


struct BookModel: Codable {
    static var BOOK_DEFAULT = "BOOK_DEFAULT"
    enum Position: String {
        case inDoor = "IN-Door"
        case outDoor = "OUT-Door"
    }
    var isCover: Bool?
    var persons: Int?
    var childs: Int?
    var typeID: Int?
    var position: String?
    var date: String?
    var QRCODE: Bool?
    var resturantID: Int?
    var menu: [FoodMenuModel.MenuItem]?
    static func instance() -> BookModel {
        let data = UserDefaults.standard.data(forKey: BOOK_DEFAULT)
        let model = try? JSONDecoder().decode(BookModel.self, from: data ?? Data())
        if let model = model {
            return model
        } else {
            let model = BookModel(isCover: false, persons: 0, childs: 0, typeID: nil, position: BookModel.Position.inDoor.rawValue, date: nil, QRCODE: false, resturantID: nil, menu: [])
            return model
        }
    }
    func save() {
        let data = try? JSONEncoder().encode(self)
        UserDefaults.standard.set(data ?? Data(), forKey: BookModel.BOOK_DEFAULT)
    }
    static func reset() {
        UserDefaults.standard.set(Data(), forKey: BOOK_DEFAULT)
    }
//    func fetch() -> BookModel {
//        return BookModel()
//    }
    
    static func updateMenu(menu: FoodMenuModel.MenuItem?, qty: Int) {
        var book = self.instance()
        if book.menu == nil {
            book.menu = []
        }
        var counter = 0
        var checker = false
        for item in book.menu ?? [] {
            if item.id == menu?.id {
                checker = true
                break
            }
            counter += 1
        }
        if !checker {
            guard let menu = menu else { return }
            var model = menu
            model.qty = qty
            book.menu?.append(model)
        } else {
            guard let menu = menu else { return }
            var model = menu
            model.qty = qty
            book.menu?.remove(at: counter)
            book.menu?.insert(model, at: counter)
        }
        
        book.save()
    }
}
