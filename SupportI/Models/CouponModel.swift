// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let couponModel = try? newJSONDecoder().decode(CouponModel.self, from: jsonData)

import Foundation

// MARK: - CouponModel
struct CouponModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: DataClass?
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let id: Int?
        let price, featuredDueDate, featuredStartDate: String?
        let isPublished, placeID, count: Int?
        let createdAt, updatedAt: String?
        let offerID: Int?
        let name, locale: String?
        
        enum CodingKeys: String, CodingKey {
            case id, price
            case featuredDueDate = "featured_due_date"
            case featuredStartDate = "featured_start_date"
            case isPublished = "is_published"
            case placeID = "place_id"
            case count
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case offerID = "offer_id"
            case name, locale
        }
    }

    
}

