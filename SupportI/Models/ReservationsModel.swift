// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let reservationsModel = try? newJSONDecoder().decode(ReservationsModel.self, from: jsonData)

import Foundation

struct ReservationSingleModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: ReservationsModel.Datum?
}
// MARK: - ReservationsModel
struct ReservationsModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: [Datum]?
    
    
    // MARK: - Datum
    struct Datum: Codable {
        let id: Int?
        let customer: Customer?
        let place: GatheringType?
        let table: Table?
        let gatheringType: GatheringType?
        let status, statusMessage: String?
        let adultCount, childrenCount: Int?
        let notes, createdAt, date, startAt: String?
        let mechanism, customerName, customerMobile: String?
        let canSendQueueReservationNotification: Bool?
        let price: String?
        let rated: Int?
        
        enum CodingKeys: String, CodingKey {
            case id, customer, place, table
            case gatheringType = "gathering_type"
            case status
            case statusMessage = "status_message"
            case adultCount = "adult_count"
            case childrenCount = "children_count"
            case notes
            case createdAt = "created_at"
            case date
            case startAt = "start_at"
            case mechanism
            case customerName = "customer_name"
            case customerMobile = "customer_mobile"
            case canSendQueueReservationNotification = "can_send_queue_reservation_notification"
            case price
            case rated
        }
    }
    
    // MARK: - Customer
    struct Customer: Codable {
        let id: Int?
        let name, mobile, email, type: String?
        let verified: Int?
        let gender: String?
    }
    
    // MARK: - GatheringType
    struct GatheringType: Codable {
        let id: Int?
        let name: String?
        let image: String?
        let payment_types: String?
        let barrierable: Bool?
    }
    
    // MARK: - Table
    struct Table: Codable {
        let id, number, capacityFrom, capacityTo: Int?
        let hasBarrier: Int?
        let position: String?
        let tableType: GatheringType?
        let availability: Bool?
        
        enum CodingKeys: String, CodingKey {
            case id, number
            case capacityFrom = "capacity_from"
            case capacityTo = "capacity_to"
            case hasBarrier = "has_barrier"
            case position
            case tableType = "table_type"
            case availability
        }
    }

    
}
