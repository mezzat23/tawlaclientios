// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let homeModel = try? newJSONDecoder().decode(HomeModel.self, from: jsonData)

import Foundation

// MARK: - HomeModel
struct HomeModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let placeTypes: [PlaceType]?
    let bestPlace: [BestPlace]?
    let offers: [Offer]?
    let serviceTypes: [PlaceType]?
    enum CodingKeys: String, CodingKey {
        case status, error, message
        case placeTypes = "place_types"
        case bestPlace = "best_place"
        case offers
        case serviceTypes = "service_types"
    }
    // MARK: - BestPlace
    struct BestPlace: Codable {
        let id: Int?
        let name, address, dressType, priceNote: String?
        let paymentTypes, familtiesChildrenNote: String?
        let hasChildren: Bool?
        let area, city, country: String?
        let phones: [String]?
        let placeOwner: String?
        let details, workingSchedule: String?
        let rate: String?
        let latitude, longitude: String?
        let bannerLink: String?
        let image: String?
        let placeType: PlaceType?
        let ratesNum, reservationsNum: Int?
        
        enum CodingKeys: String, CodingKey {
            case id, name, address
            case dressType = "dress_type"
            case priceNote = "price_note"
            case paymentTypes = "payment_types"
            case familtiesChildrenNote = "familties_children_note"
            case hasChildren = "has_children"
            case area, city, country, phones
            case placeOwner = "place_owner"
            case details
            case workingSchedule = "working_schedule"
            case rate, latitude, longitude
            case bannerLink = "banner_link"
            case image
            case placeType = "place_type"
            case ratesNum = "rates_num"
            case reservationsNum = "reservations_num"
        }
    }
    
    // MARK: - PlaceType
    struct PlaceType: Codable {
        let id: Int?
        let name: String?
        let image: String?
        let icon: String?
        let num: Int?
    }
    
    // MARK: - Offer
    struct Offer: Codable {
        let id: Int?
        let name, price, dueDate: String?
        let image: String?
        
        enum CodingKeys: String, CodingKey {
            case id, name, price
            case dueDate = "due_date"
            case image
        }
    }

}

