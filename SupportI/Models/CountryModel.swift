// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let countryModel = try? newJSONDecoder().decode(CountryModel.self, from: jsonData)

import Foundation

// MARK: - CountryModel
struct CountryModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: [Datum]?
    // MARK: - Datum
    struct Datum: Codable {
        let id: Int?
        let name, code: String?
        let flag: String?
        let isDefault: Int?
        
        enum CodingKeys: String, CodingKey {
            case id, name, code, flag
            case isDefault = "is_default"
        }
    }

}

