// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let settingModel = try? newJSONDecoder().decode(SettingModel.self, from: jsonData)

import Foundation

// MARK: - SettingModel
struct SettingModel: Codable {
    let data: DataClass?
    // MARK: - DataClass
    struct DataClass: Codable {
        let mobile, phone, termsAndConditions, email: String?
        let website: String?
        let twitter: String?
        let facebook, snab, instagram: String?
        let vat: String?
        let about, usage: About?
        
        enum CodingKeys: String, CodingKey {
            case mobile, phone, vat
            case termsAndConditions = "terms_and_conditions"
            case email, website, twitter, facebook, snab, instagram, about, usage
        }
    }
    
    // MARK: - About
    struct About: Codable {
        let id: Int?
        let name, aboutDescription: String?
        
        enum CodingKeys: String, CodingKey {
            case id, name
            case aboutDescription = "description"
        }
    }

}

