// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let placeTypeModel = try? newJSONDecoder().decode(PlaceTypeModel.self, from: jsonData)

import Foundation

// MARK: - PlaceTypeModel
struct PlaceTypeModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: [Datum]?
    
    // MARK: - Datum
    struct Datum: Codable {
        let id: Int?
        let name: String?
        let image: String?
    }

}
