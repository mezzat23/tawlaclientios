// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let featuredOffer = try? newJSONDecoder().decode(FeaturedOffer.self, from: jsonData)

import Foundation

// MARK: - FeaturedOffer
struct FeaturedOffer: Codable {
    let data: [Datum]?
    
    // MARK: - Datum
    struct Datum: Codable {
        let id: Int?
        let name, price: String?
        let image: String?
        let place: Place?
    }
    
    // MARK: - Place
    struct Place: Codable {
        let id: Int?
        let name, address, dressType, priceNote: String?
        let paymentTypes, familtiesChildrenNote: String?
        let hasChildren: Bool?
        let area, city, country: String?
        let phones: [String]?
        let placeOwner, details, workingSchedule: String?
        let rate: Int?
        let latitude, longitude, bannerLink: String?
        let image: String?
        let placeType: PlaceType?
        let ratesNum, reservationsNum: Int?
        
        enum CodingKeys: String, CodingKey {
            case id, name, address
            case dressType = "dress_type"
            case priceNote = "price_note"
            case paymentTypes = "payment_types"
            case familtiesChildrenNote = "familties_children_note"
            case hasChildren = "has_children"
            case area, city, country, phones
            case placeOwner = "place_owner"
            case details
            case workingSchedule = "working_schedule"
            case rate, latitude, longitude
            case bannerLink = "banner_link"
            case image
            case placeType = "place_type"
            case ratesNum = "rates_num"
            case reservationsNum = "reservations_num"
        }
    }
    
    // MARK: - PlaceType
    struct PlaceType: Codable {
        let id: Int?
        let name: String?
        let image: String?
    }

}
