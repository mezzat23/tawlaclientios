// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let foodMenuModel = try? newJSONDecoder().decode(FoodMenuModel.self, from: jsonData)

import Foundation

// MARK: - FoodMenuModel
struct FoodMenuModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: DataClass?
    
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let currentPage: Int?
        let data: [Datum]?
        let firstPageURL: String?
        let from, lastPage: Int?
        let lastPageURL, path: String?
        let perPage, to, total: Int?
        
        enum CodingKeys: String, CodingKey {
            case currentPage = "current_page"
            case data
            case firstPageURL = "first_page_url"
            case from
            case lastPage = "last_page"
            case lastPageURL = "last_page_url"
            case path
            case perPage = "per_page"
            case to, total
        }
    }
    
    // MARK: - Datum
    struct Datum: Codable {
        let id: Int?
        let createdAt, updatedAt: String?
        let menuItems: [MenuItem]?
        let name: String?
        let translations: [DatumTranslation]?
        
        enum CodingKeys: String, CodingKey {
            case id
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case menuItems = "menu_items"
            case name, translations
        }
    }
    
    // MARK: - MenuItem
    struct MenuItem: Codable {
        let id: Int?
        let price: String?
        let placeID, menuCatID: Int?
        let createdAt, updatedAt: String?
        let pic: String?
        let image: String?
        let name, details: String?
        let translations: [MenuItemTranslation]?
        var qty: Int?
        enum CodingKeys: String, CodingKey {
            case id, price
            case placeID = "place_id"
            case menuCatID = "menu_cat_id"
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case image
            case pic, name, details, translations, qty
        }
    }
    
    // MARK: - MenuItemTranslation
    struct MenuItemTranslation: Codable {
        let id, menuItemID: Int?
        let name, details: String?
        let locale: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case menuItemID = "menu_item_id"
            case name, details, locale
        }
    }

    // MARK: - DatumTranslation
    struct DatumTranslation: Codable {
        let id, menuCatID: Int?
        let name: String?
        let locale: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case menuCatID = "menu_cat_id"
            case name, locale
        }
    }

    
}
