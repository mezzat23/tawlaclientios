// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let cityModel = try? newJSONDecoder().decode(CityModel.self, from: jsonData)

import Foundation

// MARK: - CityModel
struct CityModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: DataClass?
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let data: [Datum]?
    }
    
    // MARK: - Datum
    struct Datum: Codable {
        let id, countryID: Int?
        let createdAt, updatedAt, name: String?
        let translations: [Translation]?
        
        enum CodingKeys: String, CodingKey {
            case id
            case countryID = "country_id"
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case name, translations
        }
    }
    
    // MARK: - Translation
    struct Translation: Codable {
        let id, cityID: Int?
        let name: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case cityID = "city_id"
            case name
        }
    }
}
