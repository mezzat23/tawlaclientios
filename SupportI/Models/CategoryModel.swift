// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let categoryModel = try? newJSONDecoder().decode(CategoryModel.self, from: jsonData)

import Foundation

// MARK: - CategoryModel
struct CategoryModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: DataClass?
    
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let currentPage: Int?
        let data: [Datum]?
        let total: Int?
        
        enum CodingKeys: String, CodingKey {
            case currentPage = "current_page"
            case data, total
        }
    }
    
    // MARK: - Datum
    struct Datum: Codable {
        let id: Int?
        let createdAt, updatedAt, name: String?
        let translations: [Translation]?
        
        enum CodingKeys: String, CodingKey {
            case id
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case name, translations
        }
    }
    
    // MARK: - Translation
    struct Translation: Codable {
        let id, menuCatID: Int?
        let name, locale: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case menuCatID = "menu_cat_id"
            case name, locale
        }
    }
}

