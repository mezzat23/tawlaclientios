// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let orderCompleteModel = try? newJSONDecoder().decode(OrderCompleteModel.self, from: jsonData)

import Foundation

// MARK: - OrderCompleteModel
struct OrderCompleteModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: DataClass?
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let resID: Int?
        let resNum, resDate, resTime, resPrice: String?
        let resPayment: String?
        
        enum CodingKeys: String, CodingKey {
            case resID = "res_id"
            case resNum = "res_num"
            case resDate = "res_date"
            case resTime = "res_time"
            case resPrice = "res_price"
            case resPayment = "res_payment"
        }
    }

}
