import CoreData
import UIKit

public func api(_ method: EndPoint, _ paramters: [Any] = []) -> String {
    var url = method.rawValue
    for key in paramters {
        url += "/\(key)"
    }
    return url
}

extension BaseApi {
    func safeUrl(url: String) -> String {
        let safeURL = url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        return safeURL
    }
    func initGet(method: String) -> String {
        var genericUrl: String = method
        var counter = 0
        if self.paramaters.count > 0 {
            for (key, value) in self.paramaters {
                if counter == 0 {
                    genericUrl += "?"+key+"=\(value)"
                } else {
                    genericUrl += "&"+key+"=\(value)"
                }
                counter += 1
            }
        }
        return genericUrl
    }

    func setErrorMessage(data: Data?) {
        guard data != nil else { return }
        guard let error = try? JSONDecoder().decode(BaseModel.self, from: data ?? Data()) else { return }
        if let errors = error.errors {
            if error.errors == nil {
                if error.message != nil {
                    errors.message = error.message
                } else {
                    errors.message = errors.description()
                }
                self.makeAlert(errors.message ?? "", closure: {})
            } else {
                errors.message = errors.description()
                self.makeAlert(errors.message ?? "", closure: {})
            }
          
        } else {
            if error.message != nil {
                self.makeAlert(error.message!, closure: {})
            }
        }
    }
    func setErrorModelMessage(data: Data?) {
        guard data != nil else { return }
        guard let error = try? JSONDecoder().decode(ErrorModel.self, from: data ?? Data()) else { return }
        if let errors = error.errors {
            self.makeAlert(errors.description(), closure: {})
        } else if let errors = error.message {
            self.makeAlert(errors.description(), closure: {})
        }
        //        else {
        //            if error.message != nil {
        //                self.makeAlert(error.message!, closure: {})
        //            }
        //        }
    }
}
