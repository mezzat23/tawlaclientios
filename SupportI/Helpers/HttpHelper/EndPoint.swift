//
//  EndPoint.swift
//  SupportI
//
//  Created by Mohamed Abdu on 3/20/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import Foundation

public enum EndPoint: String {
    case token
    case home
    case featuredOffers = "featured_offers"
    case cities
    case places = "places"
    case login = "auth/login"
    case register = "auth/register"
    case forget = "auth/password/forget"
    case reset = "auth/password/reset"
    case logout = "auth/password/logout"
    case verifyCode = "auth/mobile-activation"
    case resendCode = "auth/resend-code"
    case profile = "userProfile"
    case updateProfile
    case favorites
    case notifications
    case sliders
    case cats
    case tablesTypes = "gathering_types"
    case reservations
    case settings
    case pages = "static_pages"
    case page
    case services = "place_types"
    case countries
    case coupon = "coupon_exist"
    case walletCheck = "walletCheck"
    case wallet = "wallet"
}
