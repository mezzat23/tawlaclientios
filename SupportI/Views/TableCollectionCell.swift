//
//  TableCollectionCell.swift
//  SupportI
//
//  Created by M.abdu on 7/4/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

protocol TablesCollectionDelagate: class {
    func didAddNew()
}
class TableCollectionCell: UICollectionViewCell, CellProtocol {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var checkImage: UIImageView!
    
    weak var delegate: TablesCollectionDelagate?
    func setup() {
        guard let model = model as? TablesModel.Datum else { return }
        imageView.setImage(url: model.tableType?.pic)
        titleLbl.text = model.position
    }

}
