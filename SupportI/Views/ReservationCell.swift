//
//  ReservationCell.swift
//  Tawla-client
//
//  Created by M.abdu on 7/9/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

protocol ReservationCellDelegate: class {
    func didEdit(path: Int)
    func didCancel(path: Int)
    func didRate(path: Int)
    func didReOrder(path: Int)
}
class ReservationCell: UITableViewCell, CellProtocol {
    @IBOutlet weak var resturantImage: UIImageView!
    @IBOutlet weak var resturantNameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var typeCountLbl: UILabel!
    @IBOutlet weak var tableNumLbl: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    weak var delegate: ReservationCellDelegate?
    var isPrev: Bool = false
    func setup() {
        guard let model = model as? ReservationsModel.Datum else { return }
        resturantImage.setImage(url: model.place?.image)
        resturantNameLbl.text = model.place?.name
        dateLbl.text = "\(model.date ?? "") \(model.startAt ?? "")"

        typeLbl.text = model.gatheringType?.name
        typeCountLbl.text = "\((model.adultCount ?? 0) + (model.childrenCount ?? 0))"
        tableNumLbl.text = model.table?.number?.string ?? "0"
        if isPrev {
            editBtn.isHidden = false
            if model.rated == 1 {
                editBtn.isHidden = true
            }
            editBtn.setImage(#imageLiteral(resourceName: "star"), for: .normal)
            editBtn.setTitle("Rate Book".localized, for: .normal)
            
            cancelBtn.isHidden = false
            cancelBtn.setImage(#imageLiteral(resourceName: "bookings_nav-1"), for: .normal)
            cancelBtn.setTitle("Reorder Book".localized, for: .normal)
        } else {
            editBtn.isHidden = true
            editBtn.setImage(#imageLiteral(resourceName: "booking_edit"), for: .normal)
            editBtn.setTitle("Edit Book".localized, for: .normal)
            
            if model.status == "approved" || model.status == "canceled" {
                cancelBtn.isHidden = true
            } else {
                cancelBtn.isHidden = false
            }
            cancelBtn.setImage(#imageLiteral(resourceName: "delete"), for: .normal)
            cancelBtn.setTitle("Cancel Book".localized, for: .normal)
        }
    }
    @IBAction func editBook(_ sender: Any) {
        if isPrev == true {
            delegate?.didRate(path: indexPath())
        } else {
            delegate?.didEdit(path: indexPath())
        }
    }
    @IBAction func cancelBook(_ sender: Any) {
        if isPrev == true {
            delegate?.didReOrder(path: indexPath())
        } else {
            delegate?.didCancel(path: indexPath())
        }
    }

    
}
