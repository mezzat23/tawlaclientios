//
//  Resturants.swift
//  SupportI
//
//  Created by Adam on 3/19/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import Cosmos
class Resturants: UITableViewCell, CellProtocol {
    @IBOutlet weak var resturantImage: UIImageView!
    @IBOutlet weak var resturantLocation: UILabel!
    @IBOutlet weak var resturantName: UILabel!
    @IBOutlet weak var resturantCategory: UILabel!
    @IBOutlet weak var rate: CosmosView!
    @IBOutlet weak var ratesLbl: UILabel!
    @IBOutlet weak var reserveLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var discountDescLbl: UILabel!
    @IBOutlet weak var menuCollection: UICollectionView! {
        didSet {
            menuCollection.delegate = self
            menuCollection.dataSource = self
        }
    }
    func setup() {
        guard let model = model as? AllResturantModel.Datum else { return }
        resturantImage.setImage(url: model.image)
        resturantLocation.text = "$$$ . \(model.country ?? "") - \(model.city ?? "") - \(model.area ?? "")"
        resturantName.text = model.name
        resturantCategory.text = model.details
        rate.rating = model.rate?.double() ?? 0
        ratesLbl.text = "\("rates".localized): \(model.ratesNum ?? 0)"
        reserveLbl.text = "\("Reserved".localized) \(model.reservationsNum ?? 0) \("time this day".localized)"
        discountLbl.text = "\(model.offers?.first?.price ?? "")"
        discountDescLbl.text = "\(model.offers?.first?.name ?? "")"
    }
}
extension Resturants: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 180, height: 65)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let model = model as? AllResturantModel.Datum else { return 0 }
        return model.menuItems?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = collectionView.cell(type: Resturentproducts.self, indexPath)
        guard let model = model as? AllResturantModel.Datum else { return UICollectionViewCell() }
        cell.model = model.menuItems?[indexPath.row]
        return cell
    }
    
}
