//
//  Foodmenufooter.swift
//  SupportI
//
//  Created by Adam on 3/19/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

protocol MenuDelegate: class {
    func plus(path: Int)
    func min(path: Int)
}

class MenuItemQuantity: UITableViewCell, CellProtocol {
    @IBOutlet weak var itemDescLbl: UILabel!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var qtyLbl: UILabel!
    @IBOutlet weak var shppingView: UIView!
    @IBOutlet weak var priceLbl: UILabel!
    
    weak var delegate: MenuDelegate?
    var qty: Int = 0
    var menu: FoodMenuModel.MenuItem?
    var disable: Bool = false
    func setup() {
        qtyLbl.text = qty.string
    }
    
    @IBAction func plus(_ sender: Any) {
        if disable {
            return
        }
        qty += 1
        qtyLbl.text = qty.string
        BookModel.updateMenu(menu: menu, qty: qty)
    }
    @IBAction func min(_ sender: Any) {
        if disable {
            return
        }
        if qty == 1 {
            return
        }
        qty -= 1
        qtyLbl.text = qty.string
        BookModel.updateMenu(menu: menu, qty: qty)
    }
}
