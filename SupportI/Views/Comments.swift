//
//  Comments.swift
//  SupportI
//
//  Created by Adam on 3/22/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import Cosmos
class Comments: UITableViewCell, CellProtocol {
    @IBOutlet weak var commentDateLbl: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var userRate: CosmosView!
    @IBOutlet weak var commentDetailsLbl: UILabel!

    func setup() {
        guard let model = model as? AllResturantModel.Rate else { return }
        commentDateLbl.text = model.createdAt
        userImage.setImage(url: model.user?.image)
        usernameLbl.text = model.user?.name
        userRate.rating = model.placeRate ?? 0
        commentDetailsLbl.text = "test test test"
    }
}
