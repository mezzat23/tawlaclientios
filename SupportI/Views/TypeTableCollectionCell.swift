//
//  TypeTableCollectionCell.swift
//  Tawla-client
//
//  Created by M.abdu on 7/7/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class TypeTableCollectionCell: UICollectionViewCell, CellProtocol {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var containView: UIView!
    func setup() {
        guard let model = model as? TableTypeModel.Datum else { return }
        titleLbl.text = model.name
        imageView.setImage(url: model.image)
    }

}
