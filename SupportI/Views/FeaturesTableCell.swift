//
//  FeaturesTableCell.swift
//  Tawla-client
//
//  Created by M.abdu on 7/18/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class FeaturesTableCell: UITableViewCell, CellProtocol {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    func setup() {
        guard let model = model as? AllResturantModel.PlaceType else { return }
        titleLbl.text = model.name
        iconView.setImage(url: model.image)
        
    }
    
}
