//
//  Imagehomeslider.swift
//  SupportI
//
//  Created by Adam on 3/18/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class Imagehomeslider: UICollectionViewCell, CellProtocol {
    @IBOutlet weak var sliderImage: UIImageView!
    
    func setup() {
        guard let model = model as? String else { return }
        sliderImage.setImage(url: model)
    }
}
