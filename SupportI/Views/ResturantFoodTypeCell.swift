//
//  ResturantFoodTypeCell.swift
//  Tawla-client
//
//  Created by M.abdu on 5/25/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class ResturantFoodTypeCell: UICollectionViewCell, CellProtocol {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var backView: UIView!
    
    func setup() {
        guard let model = model as? AllResturantModel.PlaceType else { return }
        titleLbl.text = model.name
    }
}
