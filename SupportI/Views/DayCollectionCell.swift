//
//  DayCollectionCell.swift
//  SupportI
//
//  Created by M.abdu on 7/4/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class DayCollectionCell: UICollectionViewCell, CellProtocol {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var containView: UIView!
    
    var day: AllResturantModel.WorkingDay?
    var isChecked: Bool = false
    func setup() {
        let name = day?.day
        nameLbl.text = name
        if isChecked {
            nameLbl.textColor = .white
            containView.backgroundColor = .appColor
        } else {
            nameLbl.textColor = .appDarkGrey
            containView.backgroundColor = .textFieldBackGround
        }
       
       
    }
}
