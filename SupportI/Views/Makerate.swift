//
//  Makerate.swift
//  SupportI
//
//  Created by Adam on 3/30/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import Cosmos
protocol MakeRateDelegate: class {
    func didChangeRate(path: Int, rate: Double)
}
class Makerate: UITableViewCell, CellProtocol {
    @IBOutlet weak var rateLbl: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    
    weak var delegate: MakeRateDelegate?
    func setup() {
        rateView.didFinishTouchingCosmos = { rate in
            self.delegate?.didChangeRate(path: self.indexPath(), rate: rate)
        }
    }
}
