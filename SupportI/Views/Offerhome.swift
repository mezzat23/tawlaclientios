//
//  Offerhome.swift
//  SupportI
//
//  Created by Adam on 3/18/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class Offerhome: UICollectionViewCell, CellProtocol {
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var offerImage: UIImageView!
    @IBOutlet weak var offerName: UILabel!
    @IBOutlet weak var offerDescription: UILabel!
    
    func setup() {
        guard let model = model as? HomeModel.Offer else { return }
        timeLbl.text = "\("End after".localized) \(model.dueDate ?? "")"
        offerImage.setImage(url: model.image)
        offerName.text = model.name
        offerDescription.text = model.price
    }
}
