//
//  Foodmenufooter.swift
//  SupportI
//
//  Created by Adam on 3/19/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class Foodmenufooter: UITableViewCell, CellProtocol {
    @IBOutlet weak var itemDescLbl: UILabel!
    @IBOutlet weak var itemPriceLbl: UILabel!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
    func setup() {
        guard let model = model as? AllResturantModel.MenuItem else { return setupFoodMenu() }
        itemImage.setImage(url: model.image)
        itemNameLbl.text = model.name
        itemPriceLbl.text = model.price
        itemDescLbl.text = model.details
    }
    func setupFoodMenu() {
        guard let model = model as? FoodMenuModel.Datum else { return }
//        itemImage.setImage(url: model.pic)
//        itemNameLbl.text = model.name
//        itemPriceLbl.text = model.price
//        itemDescLbl.text = model.details
    }
}
