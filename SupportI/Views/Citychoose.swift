//
//  Citychoose.swift
//  SupportI
//
//  Created by Adam on 3/18/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

protocol CityCellDelegate: class {
    func onSelectCity(path: Int)
    func onDeSelectCity(path: Int)
}
class Citychoose: UITableViewCell, CellProtocol {
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var selectBtn: RadioButton!
    
    weak var delegate: CityCellDelegate?
    var isChecked: Bool = false
    func setup() {
        guard let model = model as? CityModel.Datum else { return }
        cityLbl.text = model.name
        if isChecked {
            selectBtn.select()
        } else {
            selectBtn.deselect()
        }
        selectBtn.onSelect { [weak self] in
            self?.delegate?.onSelectCity(path: self?.path ?? 0)
        }
        selectBtn.onDeselect { [weak self] in
            self?.delegate?.onDeSelectCity(path: self?.path ?? 0)
        }
    }
}
