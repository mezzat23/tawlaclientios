//
//  Locationhome.swift
//  SupportI
//
//  Created by Adam on 3/18/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import Cosmos
class Locationhome: UICollectionViewCell, CellProtocol {
    @IBOutlet weak var placeImage: UIImageView!
    @IBOutlet weak var placeType: UILabel!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var typesLbl: UILabel!
    @IBOutlet weak var rate: CosmosView!
    @IBOutlet weak var ratesLbl: UILabel!
    @IBOutlet weak var reservedLbl: UILabel!
    @IBOutlet weak var favoriteBtn: UIButton!
    
    var closureFavorite: ((Int) -> Void)?
    func setup() {
        guard let model = model as? HomeModel.BestPlace else { return setupResturantModel() }
        placeImage.setImage(url:  model.image)
        //placeType.text = "$$$ . \(model.city ?? "") - \(model.placeType?.name ?? "")"
        placeType.text = "$$$ . \(model.country ?? "") - \(model.city ?? "") - \(model.area ?? "")"

        placeName.text = model.name
        typesLbl.text = model.details
        rate.rating = model.rate?.double() ?? 0
        ratesLbl.text = "\("rates".localized): \(model.ratesNum ?? 0)"
        reservedLbl.text = "\("Reserved".localized) \(model.reservationsNum ?? 0) \("time this day".localized)"
        favoriteBtn.isHidden = true
        favoriteBtn.UIViewAction {
            self.closureFavorite?(self.indexPath())
        }
    }
    func setupResturantModel() {
        guard let model = model as? AllResturantModel.Datum else { return }
        placeImage.setImage(url:  model.image)
        placeType.text = "$$$ . \(model.city ?? "") - \(model.placeType?.name ?? "")"
        placeName.text = model.name
        typesLbl.text = model.details
        rate.rating = model.rate?.double() ?? 0
        ratesLbl.text = "\("rates".localized): \(model.ratesNum ?? 0)"
        reservedLbl.text = "\("Reserved".localized) \(model.reservationsNum ?? 0) \("time this day".localized)"
        favoriteBtn.UIViewAction {
            self.closureFavorite?(self.indexPath())
        }
    }
}
