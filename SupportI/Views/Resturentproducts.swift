//
//  Resturentproducts.swift
//  SupportI
//
//  Created by Adam on 3/19/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class Resturentproducts: UICollectionViewCell, CellProtocol {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
    func setup() {
        guard let model = model as? AllResturantModel.MenuItem else { return }
        productImage.setImage(url: model.image)
        productName.text = model.name
        productPrice.text = model.price
    }
}
