//
//  Walletviewcell.swift
//  SupportI
//
//  Created by Adam on 3/24/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class Walletviewcell: UITableViewCell, CellProtocol {
    @IBOutlet weak var typeImage: UIImageView!
    @IBOutlet weak var tableLbl: UILabel!
    @IBOutlet weak var resturantLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    func setup() {
        guard let model = model as? WalletModel.Datum else { return }
        if model.type == "in" {
            typeImage.image = #imageLiteral(resourceName: "output")
        } else {
            typeImage.image = #imageLiteral(resourceName: "input")
        }
        tableLbl.text = model.txt
        resturantLbl.text = model.note
        dateLbl.text = model.createdAt
        priceLbl.text = "\(model.value ?? "") \("SAR".localized)"
    }
}
