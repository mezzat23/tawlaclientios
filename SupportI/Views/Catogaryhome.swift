//
//  Catogaryhome.swift
//  SupportI
//
//  Created by Adam on 3/18/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class Catogaryhome: UICollectionViewCell, CellProtocol {
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    
    func setup() {
        guard let model = model as? HomeModel.PlaceType else { return }
        categoryImage.setImage(url: model.image)
        categoryTitle.text = model.name
    }
}
