//
//  OnboardingCell.swift
//  Tawla-client
//
//  Created by M.abdu on 7/18/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class OnboardingCell: UICollectionViewCell, CellProtocol {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    
    func setup() {
        
    }
}
