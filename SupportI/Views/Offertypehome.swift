//
//  Offertypehome.swift
//  SupportI
//
//  Created by Adam on 3/18/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class Offertypehome: UICollectionViewCell, CellProtocol {
    @IBOutlet weak var offerImage: UIImageView!
    @IBOutlet weak var offerTitle: UILabel!
    @IBOutlet weak var resturantCount: UILabel!

    func setup() {
        guard let model = model as? HomeModel.PlaceType else { return }
        offerImage.setImage(url: model.image)
        offerTitle.text = model.name
        resturantCount.text = model.num?.string
        
    }
}
