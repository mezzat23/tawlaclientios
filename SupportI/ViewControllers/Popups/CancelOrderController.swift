//
//  Ordercancel.swift
//  SupportI
//
//  Created by Adam on 3/30/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class CancelOrderController: BaseController {
    @IBOutlet weak var cancelTitleLbl: UILabel!
    @IBOutlet weak var cancelBodyLbl: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    
    var isAccept: Bool = false
    var reservationID: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
    func setup() {
        if isAccept {
            cancelTitleLbl.text = "Accept the reservation".localized
            cancelBodyLbl.text = "Are you sure to accept the reservation?".localized
            cancelBtn.setTitle("Accept the reservation".localized, for: .normal)
        }
    }
    func reject() {
        let method = api(.reservations, [reservationID ?? 0, "cancelled"])
        startLoading()
        ApiManager.instance.connection(method, type: .put) { (response) in
            let data = try? JSONDecoder().decode(ReservationSingleModel.self, from: response ?? Data())
            self.makeWithoutCancelAlert(data?.data?.statusMessage ?? "", closure: {
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
    func accept() {
        let method = api(.reservations, [reservationID ?? 0, "approved"])
        startLoading()
        ApiManager.instance.connection(method, type: .put) { (response) in
            let data = try? JSONDecoder().decode(ReservationSingleModel.self, from: response ?? Data())
            self.makeWithoutCancelAlert(data?.data?.statusMessage ?? "", closure: {
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
    @IBAction func cancelReservation(_ sender: Any) {
        if isAccept {
            accept()
        } else {
            reject()
        }
    }
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
