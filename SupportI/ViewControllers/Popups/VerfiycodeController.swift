//
//  Verfiycode.swift
//  SupportI
//
//  Created by Adam on 3/19/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class VerfiycodeController: BaseController {
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var code1Txf: UITextField!
    @IBOutlet weak var code2Txf: UITextField!
    @IBOutlet weak var code3Txf: UITextField!
    @IBOutlet weak var code4Txf: UITextField!
    @IBOutlet weak var btnsStack: UIStackView!
    
    var mobile: String?
    var password: String?
    var code: String {
        return UserRoot.fetch()?.mobile_verification_code?.string ?? "0000"
    }
    var codeEntered: String {
        if Localizer.current == "ar" {
            return "\(code4Txf.text ?? "")\(code3Txf.text ?? "")\(code2Txf.text ?? "")\(code1Txf.text ?? "")"
        } else {
            return "\(code1Txf.text ?? "")\(code2Txf.text ?? "")\(code3Txf.text ?? "")\(code4Txf.text ?? "")"
        }
    }
    var timerHelper: TimeHelper?
    weak var delegate: LoginDelegate?
    var prev: Bool = false
    var closurePrevVC: HandlerView?
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
  
    func setup() {
        timerHelper = .init(seconds: 1, numberOfCycle: 120, closure: { [weak self] (cycle) in
            if cycle >= 60 {
                self?.timeLbl.text = "1:\(cycle-60)"
            } else {
                self?.timeLbl.text = "0:\(cycle)"
            }
            if cycle == 0 {
                self?.timeLbl.isHidden = true
                self?.btnsStack.isHidden = false
            }
        })
        code1Txf.delegate = self
        code2Txf.delegate = self
        code3Txf.delegate = self
        code4Txf.delegate = self
        code1Txf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        code2Txf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        code3Txf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        code4Txf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
    }
    override func backBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func resend(_ sender: Any) {
        self.timeLbl.text = ""
        self.timeLbl.isHidden = false
        self.btnsStack.isHidden = true
        self.timerHelper?.stopTimer()
        self.timerHelper = nil
        self.timerHelper = .init(seconds: 1, numberOfCycle: 120, closure: { [weak self] (cycle) in
            if cycle >= 60 {
                self?.timeLbl.text = "1:\(cycle-60)"
            } else {
                self?.timeLbl.text = "0:\(cycle)"
            }
            if cycle == 0 {
                self?.timeLbl.isHidden = true
                self?.btnsStack.isHidden = false
            }
        })
        startLoading()
        ApiManager.instance.paramaters["mobile"] = UserRoot.fetch()?.data?.mobile
        //ApiManager.instance.paramaters["password"] = password
        ApiManager.instance.connection(.resendCode, type: .post) {[weak self] (response) in
            self?.stopLoading()
            let data = try? JSONDecoder().decode(UserRoot.self, from: response ?? Data())
            let user = UserRoot.fetch()
            user?.mobile_verification_code = data?.mobile_verification_code
            user?.save()
            
        }
    }
    @IBAction func verify(_ sender: Any) {
        if codeEntered != code {
            return makeWithoutCancelAlert("You entered wrong code please try again !".localized, closure: {})
        }
        startLoading()
        ApiManager.instance.paramaters["mobile"] = UserRoot.fetch()?.data?.mobile
        //ApiManager.instance.paramaters["password"] = password
        ApiManager.instance.paramaters["code"] = codeEntered
        ApiManager.instance.connection(.verifyCode, type: .post) {[weak self] (response) in
            self?.stopLoading()
            let user = UserRoot.fetch()
            user?.data?.verified = 1
            user?.save()
            self?.dismiss(animated: true) {
                self?.delegate?.didVerify()
            }
        }
    }
    @IBAction func backToPrev(_ sender: Any) {
        dismiss(animated: true) {
            if self.prev {
                self.closurePrevVC?()
            }
        }
    }
}

extension VerfiycodeController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
            case code1Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code1Txf.text = ""
                    }
            }
            case code2Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code2Txf.text = ""
                    }
            }
            case code3Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code3Txf.text = ""
                    }
            }
            case code4Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code4Txf.text = ""
                    }
            }
            default:
                break
        }
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        if Localizer.current == "ar" {
            setupArabic(textField)
            return
        }
        switch textField {
            case code1Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code2Txf.becomeFirstResponder()
                    } else {
                        view.endEditing(true)
                    }
            }
            case code2Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code3Txf.becomeFirstResponder()
                    } else {
                        code1Txf.becomeFirstResponder()
                    }
            }
            case code3Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code4Txf.becomeFirstResponder()
                    } else {
                        code2Txf.becomeFirstResponder()
                    }
            }
            case code4Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        textField.endEditing(true)
                    } else {
                        code3Txf.becomeFirstResponder()
                    }
            }
            default:
                break
        }
    }
    func setupArabic(_ textField: UITextField) {
        switch textField {
         
            case code4Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code3Txf.becomeFirstResponder()
                    } else {
                        view.endEditing(true)
                    }
            }
            case code3Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code2Txf.becomeFirstResponder()
                    } else {
                        code4Txf.becomeFirstResponder()
                    }
            }
            case code2Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code1Txf.becomeFirstResponder()
                    } else {
                        code3Txf.becomeFirstResponder()
                    }
            }
            case code1Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        return
                    } else {
                        code2Txf.becomeFirstResponder()
                    }
            }
            default:
                break
        }
    }
}
