//
//  Citychoose.swift
//  SupportI
//
//  Created by Adam on 3/18/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class SelectCityController: BaseController {

    @IBOutlet weak var citiesTbl: UITableView! {
        didSet {
            citiesTbl.delegate = self
            citiesTbl.dataSource = self
        }
    }
    var source: [CityModel.Datum] = []
    var onSelectCity: ((Int) -> Void)?
    var selectedCity: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            if self.selectedCity != nil {
                self.onSelectCity?(self.selectedCity ?? 0)
            }
        })
    }
}
// MARK:  TableView
extension SelectCityController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return source.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: Citychoose.self, indexPath)
        if indexPath.row == selectedCity {
            cell.isChecked = true
        } else {
            cell.isChecked = false
        }
        cell.delegate = self
        cell.model = source[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedCity == indexPath.row {
            self.onDeSelectCity(path: indexPath.row)
        } else {
            self.onSelectCity(path: indexPath.row)
        }
    }
}
// MARK:  City Delegate
extension SelectCityController: CityCellDelegate {
    func onSelectCity(path: Int) {
        selectedCity = path
        citiesTbl.reloadData()
    }
    func onDeSelectCity(path: Int) {
        if selectedCity == path {
             selectedCity = nil
        }
    }
}
