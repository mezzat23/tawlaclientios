//
//  Changepassword.swift
//  SupportI
//
//  Created by Adam on 3/22/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

protocol ChangePasswordDelegate: class {
    func didChangePassword(password: String)
}
class ChangePasswordController: BaseController {
    @IBOutlet weak var currentPasswordTxf: UITextField!
    @IBOutlet weak var newPasswordTxf: UITextField!
    @IBOutlet weak var confirmationPasswordTxf: UITextField!
    
    weak var delegate: ChangePasswordDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
    
    func setup() {
        
    }
    override func backBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func save(_ sender: Any) {
        if !validate(txfs: [currentPasswordTxf, newPasswordTxf, confirmationPasswordTxf]) {
            return
        }
        if newPasswordTxf.text != confirmationPasswordTxf.text {
            makeAlert("Password doesn't match".localized, closure: {})
            return
        }
        self.dismiss(animated: true) {
            self.delegate?.didChangePassword(password: self.newPasswordTxf.text ?? "")
        }
    }
    @IBAction func showCurrentPassword(_ sender: Any) {
        currentPasswordTxf.isSecureTextEntry = !currentPasswordTxf.isSecureTextEntry
    }
    @IBAction func showNewPassword(_ sender: Any) {
        newPasswordTxf.isSecureTextEntry = !newPasswordTxf.isSecureTextEntry
    }
    @IBAction func showConfirmationPassword(_ sender: Any) {
        confirmationPasswordTxf.isSecureTextEntry = !confirmationPasswordTxf.isSecureTextEntry
    }
}
