//
//  Qrcode.swift
//  SupportI
//
//  Created by Adam on 3/26/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import AVFoundation

protocol QrCodeDelegate: class {
    func didFindPlace(for place: String?)
    func didFindTable(for table: ReservationsModel.Datum?)
}
class QrcodeController: BaseController {
    @IBOutlet weak var codeView: UIView!
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    weak var delegate: QrCodeDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    func setup() {
        codeView.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = codeView.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        codeView.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
        // Do any additional setup after loading the view.
    }
  
    override func backBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func scanAgain(_ sender: Any) {
        captureSession.startRunning()
    }
    
}

extension QrcodeController: AVCaptureMetadataOutputObjectsDelegate {
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            checkPlace(code: stringValue)
        }
    }
    func checkPlace(code: String) {
        startLoading()
        var check = ""
        if code.contains("-T") {
            check = code.cut(charSplit: "-T", charWith: "")
            self.checkTable(code: check)
        } else {
            check = code.cut(charSplit: "-R", charWith: "")
            let method = api(.places, ["check",check])
            ApiManager.instance.connection(method, type: .get) { (response) in
                self.stopLoading()
                let data = try? JSONDecoder().decode(BaseModel.self, from: response ?? Data())
                if case data?.error = false {
                    self.dismiss(animated: true) {
                        self.delegate?.didFindPlace(for: check)
                    }
                    
                }
            }
        }
        
    }
    func checkTable(code: String) {
        let method = api(.reservations, ["check",code])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(ReservationSingleModel.self, from: response ?? Data())
            if case data?.error = false {
                self.dismiss(animated: true) {
                    self.delegate?.didFindTable(for: data?.data)
                }
                
            }
        }
    }
}
