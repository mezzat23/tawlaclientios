//
//  Bookingfilter.swift
//  SupportI
//
//  Created by Adam on 3/18/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
protocol BookingPopUpDelegate: class {
    func didBook()
}
class BookingPopUpController: BaseController {
    
    @IBOutlet weak var typeCollection: UICollectionView!
    @IBOutlet weak var personsLbl: UILabel!
    @IBOutlet weak var childsLbl: UILabel!
    @IBOutlet weak var positionLbl: UILabel!
    @IBOutlet weak var positionView: UIView!
    @IBOutlet weak var coverRadioBtn: RadioButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var types: [TableTypeModel.Datum] = []
    var bookModel: BookModel?
    var selectedType: Int?
    weak var delegate: BookingPopUpDelegate?
    var QRCODE: Bool = false
    var resturantID: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchTypes()
        setup()
        handlers()
        handleBook()
        // Do any additional setup after loading the view.
    }
    func setup() {
        self.bookModel = BookModel.instance()
        self.bookModel?.resturantID = resturantID
        typeCollection.delegate = self
        typeCollection.dataSource = self
        datePicker.minimumDate = Date()
        datePicker.locale = Locale(identifier: "en")
        handleBook()
    }
    func handlers() {
        coverRadioBtn.onSelect {
            self.bookModel?.isCover = true
        }
        coverRadioBtn.onDeselect {
            self.bookModel?.isCover = false
        }
        positionView.UIViewAction {
            let scene = self.controller(PickerViewHelper.self, storyboard: .PickerViewHelper)
            scene.source = [BookModel.Position.inDoor.rawValue, BookModel.Position.outDoor.rawValue]
            scene.titleClosure = { row in
                if row == 0 {
                    return BookModel.Position.inDoor.rawValue.localized
                } else {
                    return BookModel.Position.outDoor.rawValue.localized
                }
            }
            scene.didSelectClosure = { row in
                if row == 0 {
                    self.bookModel?.position = BookModel.Position.inDoor.rawValue
                } else {
                    self.bookModel?.position = BookModel.Position.outDoor.rawValue
                }
                self.handleBook()
            }
            self.pushPop(vcr: scene)
        }
    }
    func handleBook() {
        let persons = bookModel?.persons ?? 0
        let childs = bookModel?.childs ?? 0
        let position = bookModel?.position?.localized ?? BookModel.Position.inDoor.rawValue.localized
        
        personsLbl.text = "\("Persons number :".localized) \(persons)"
        childsLbl.text = "\("Childs number :".localized) \(childs)"
        positionLbl.text = position
        
        if QRCODE {
            datePicker.isHidden = true
            self.bookModel?.QRCODE = true
            self.bookModel?.save()
        } else {
            self.bookModel?.QRCODE = false
            self.bookModel?.save()
        }
       
    }
    func setupOldType() {
        if bookModel?.typeID != nil {
            var counter = 0
            for each in types {
                if each.id == bookModel?.typeID {
                    self.selectedType = counter
                }
                counter += 1
            }
        }
    }
    override func backBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func plusPerson(_ sender: Any) {
        var persons = bookModel?.persons ?? 0
        persons += 1
        bookModel?.persons = persons
        handleBook()
    }
    @IBAction func minPerson(_ sender: Any) {
        var persons = bookModel?.persons ?? 0
        if persons == 0 {
            return
        }
        persons -= 1
        bookModel?.persons = persons
        handleBook()
    }
    @IBAction func plusChild(_ sender: Any) {
        var childs = bookModel?.childs ?? 0
        childs += 1
        bookModel?.childs = childs
        handleBook()
    }
    @IBAction func minChild(_ sender: Any) {
        var childs = bookModel?.childs ?? 0
        if childs == 0 {
            return
        }
        childs -= 1
        bookModel?.childs = childs
        handleBook()
    }
    func validate() -> Bool {
        if bookModel?.typeID == nil {
            makeAlert("Please select the type".localized, closure: {})
            return false
        }
        if bookModel?.persons == nil || bookModel?.persons == 0 {
            makeAlert("The person numbers at least 1 person".localized, closure: {})
            return false
        }
        if case bookModel?.QRCODE = true {
            
        } else {
            if bookModel?.date == nil {
                makeAlert("The booking date is required".localized, closure: {})
                return false
            }
        }
        
        return true
    }

    @IBAction func confirm(_ sender: Any) {
        let date = DateHelper().currentFullFormat(date: datePicker.date)
        let string = DateHelper().date(date: date, format: "yyyy-MM-dd HH:mm")
        bookModel?.date = string
        if !validate() {
            return
        }
        bookModel?.save()
        self.dismiss(animated: true, completion: {
            self.delegate?.didBook()
        })
    }
}
// MARK:  Netowrk
extension BookingPopUpController {
    func fetchTypes() {
        startLoading()
        ApiManager.instance.connection(.tablesTypes, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(TableTypeModel.self, from: response ?? Data())
            self.types.append(contentsOf: data?.data ?? [])
            self.setupOldType()
            self.typeCollection.reloadData()
        }
    }
}

// MARK:  Collection
extension BookingPopUpController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: collectionView.width/2 - 15, height: 48)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return types.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = collectionView.cell(type: TypeTableCollectionCell.self, indexPath)
        if selectedType == indexPath.row {
            cell.containView.borderColor = .appColor
            cell.containView.borderWidth = 1
        } else {
            cell.containView.borderWidth = 0
        }
        cell.model = types[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedType = indexPath.row
        bookModel?.typeID = types[indexPath.row].id
        collectionView.reloadData()
    }
}
