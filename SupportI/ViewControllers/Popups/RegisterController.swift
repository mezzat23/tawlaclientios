//
//  Register.swift
//  SupportI
//
//  Created by Adam on 3/19/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import CountryList
protocol RegisterDelegate: class {
    func didRegister()
}
class RegisterController: BaseController {

    @IBOutlet weak var mrBtn: UIButton!
    @IBOutlet weak var mrsBtn: UIButton!
    @IBOutlet weak var nameTxf: UITextField!
    @IBOutlet weak var mobileTxf: UITextField!
    @IBOutlet weak var passwordTxf: UITextField!
    @IBOutlet weak var emailTxf: UITextField!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var flagImage: UIImageView!

    weak var delegate: RegisterDelegate?
    var isMr: Bool = true
    var dialCode = "+966"
    override func viewDidLoad() {
        super.viewDidLoad()
        handleCountry()
        setup()
        // Do any additional setup after loading the view.
    }
    func setup() {
        mrBtn.UIViewAction {
            self.mrsBtn.backgroundColor = .lightGray
            self.mrBtn.backgroundColor = UIColor(named: "orange") ?? .orange
            self.isMr = true
        }
        mrsBtn.UIViewAction {
            self.mrBtn.backgroundColor = .lightGray
            self.mrsBtn.backgroundColor = UIColor(named: "orange") ?? .orange
            self.isMr = false
        }
    }
//    func handleCountry() {
//        countryLbl.text = "🇸🇦 966"
//        let countries = CountryList()
//        countries.delegate = self
//        countryLbl.UIViewAction {
//            let navController = UINavigationController(rootViewController: countries)
//            self.present(navController, animated: true, completion: nil)
//        }
//    }
    override func backBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func showPassword(_ sender: Any) {
        passwordTxf.isSecureTextEntry = !passwordTxf.isSecureTextEntry
    }
    @IBAction func register(_ sender: Any) {
        if mobileTxf.text?.first == "0" {
            mobileTxf.text?.removeFirst()
        }
        if validate(txfs: [nameTxf, mobileTxf, passwordTxf, emailTxf]) {
            startLoading()
            ApiManager.instance.paramaters["mobile"] = "\(dialCode)\(mobileTxf.text ?? "")"
            ApiManager.instance.paramaters["name"] = nameTxf.text
            if isMr {
                ApiManager.instance.paramaters["gender"] = "male"
            } else {
                ApiManager.instance.paramaters["gender"] = "female"
            }
            ApiManager.instance.paramaters["password"] = passwordTxf.text
            ApiManager.instance.paramaters["type"] = "customer"
            ApiManager.instance.paramaters["email"] = "\(emailTxf.text ?? "")"
            ApiManager.instance.paramaters["platform"] = "2"
            if let devicetoken = UserDefaults.standard.string(forKey: "deviceToken") {
                ApiManager.instance.paramaters["registrationid"] = devicetoken
            } else {
                ApiManager.instance.paramaters["registrationid"] = "nil"
            }
            ApiManager.instance.connection(.register, type: .post) { [weak self] (response) in
                self?.stopLoading()
                let data = try? JSONDecoder().decode(UserRoot.self, from: response ?? Data())
                data?.save()
                self?.dismiss(animated: true, completion: {
                    self?.delegate?.didRegister()
                })
            }
        }
    }
    
}
extension RegisterController: CountryListDelegate {
    func selectedCountry(country: Country) {
        self.countryLbl.text = "\(country.flag ?? "") \(country.phoneExtension)"
        dialCode = country.phoneExtension
    }
}

// MARK:  Country
extension RegisterController {
    func handleCountry() {
        if Localizer.current == "en" {
            countryLbl.text = "966 🇸🇦"
        } else {
            countryLbl.text = "🇸🇦 966"
        }
        let countries = CountryList()
        countries.delegate = self
        self.fetchCountriesSetFirst()
        countryLbl.UIViewAction {
            self.fetchCountries()
            
            //            let navController = UINavigationController(rootViewController: countries)
            //            self.present(navController, animated: true, completion: nil)
        }
        flagImage.UIViewAction {
            self.fetchCountries()
        }
    }
    func openPickerCountry(countries: [CountryModel.Datum]) {
        let scene = controller(PickerViewHelper.self, storyboard: .PickerViewHelper)
        scene.source = []
        scene.source.append(contentsOf: countries)
        scene.didSelectClosure = { row in
            self.countryLbl.text = countries[row].code
            self.dialCode = countries[row].code ?? ""
            self.flagImage.setImage(url: countries[row].flag)
        }
        scene.titleClosure = { row in
            return countries[row].name
        }
        pushPop(vcr: scene)
    }
    func fetchCountries() {
        startLoading()
        ApiManager.instance.connection(.countries, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(CountryModel.self, from: response ?? Data())
            self.openPickerCountry(countries: data?.data ?? [])
        }
    }
    func fetchCountriesSetFirst() {
        ApiManager.instance.connection(.countries, type: .get) { (response) in
            let data = try? JSONDecoder().decode(CountryModel.self, from: response ?? Data())
            let countries = data?.data ?? []
            self.countryLbl.text = countries.first?.code
            self.dialCode = countries.first?.code ?? ""
            self.flagImage.setImage(url: countries.first?.flag)
        }
    }
}
