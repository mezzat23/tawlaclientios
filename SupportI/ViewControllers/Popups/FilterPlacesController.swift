//
//  Productfilter.swift
//  SupportI
//
//  Created by Adam on 3/22/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

protocol FilterDelegate: class {
//    func didFilter(services: [Int])
//    func didFilter(categories: [Int])
    func didFilter(services: Int?, sortedBy: String?, minPrice: Int?, maxPrice: Int?)
    func didFilter(categories: Int?, sortedBy: String?, minPrice: Int?, maxPrice: Int?)
}
class FilterPlacesController: BaseController {
    @IBOutlet weak var pageTitleLbl: UILabel!
    @IBOutlet weak var servicesCollection: UICollectionView!
    @IBOutlet weak var servicesHeight: NSLayoutConstraint!
    @IBOutlet weak var sortByCollection: UICollectionView!
    @IBOutlet weak var sortsHeight: NSLayoutConstraint!
    //@IBOutlet var startViews: [UIView]!
    @IBOutlet weak var rangePrices: RangeSeekSlider!
    
    weak var delegate: FilterDelegate?
    var services: [ServiceModel.Datum] = []
    var categoryID: Int?
//    var selectedService: [Int] = []
//    var selectedCategories: [Int] = []
    var selectedService: Int?
    var selectedCategories: Int?
    var categories: [PlaceTypeModel.Datum] = []
    var sortByArray: [String] = ["Most visited", "Famous places", "Global places"]
    var selectedSortBy: Int?
    var selectedRate: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        handleRates()
        refreshRates()
        if categoryID == nil {
            fetchCategories()
        } else {
            fetchService()
        }
        // Do any additional setup after loading the view.
    }
    func setup() {
        servicesCollection.delegate = self
        servicesCollection.dataSource = self
        servicesCollection.reloadData()
        sortByCollection.delegate = self
        sortByCollection.dataSource = self
        sortByCollection.reloadData()
        sortsHeight.constant = sortByCollection.collectionViewLayout.collectionViewContentSize.height
        if categoryID == nil {
            pageTitleLbl.text = "Filter by category".localized
        } else {
            pageTitleLbl.text = "Filter by service type".localized
        }
    }
    func fetchService() {
        startLoading()
        let method = api(.services, [categoryID ?? 0, "service_types"])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(ServiceModel.self, from: response ?? Data())
            self.services.append(contentsOf: data?.data?.data ?? [])
            self.servicesCollection.reloadData()
        }
    }
    func fetchCategories() {
        startLoading()
        ApiManager.instance.connection(.services, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(PlaceTypeModel.self, from: response ?? Data())
            self.categories.append(contentsOf: data?.data ?? [])
            self.servicesCollection.reloadData()
        }
    }
    func handleRates() {
        
//        var counter = 1
//        for (index, view) in startViews.enumerated() {
//            view.UIViewAction {
//                self.selectedRate = index+1
//                self.refreshRates()
//            }
//            counter += 1
//        }
    }
    func refreshRates() {
//        var counter = 1
//        for view in startViews {
//            if counter == selectedRate {
//                view.borderColor = .appColor
//                view.backgroundColor = UIColor(red: 242/255, green: 111/255, blue: 33/255, alpha: 0.1)
//            } else {
//                view.borderColor = UIColor(red: 234/255, green: 234/255, blue: 234/255, alpha: 1)
//                view.backgroundColor = .white
//            }
//            counter += 1
//        }
    }
    override func backBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func filter(_ sender: Any) {
        var minPrice: Int?
        var maxPrice: Int?
        minPrice = rangePrices.selectedMinValue.doubleValue().int
        maxPrice = rangePrices.selectedMaxValue.doubleValue().int
        if minPrice == rangePrices.minValue.doubleValue().int && maxPrice == rangePrices.maxValue.doubleValue().int {
            minPrice = nil
            maxPrice = nil
        }
        self.dismiss(animated: true) {
            if self.categoryID == nil {
                self.delegate?.didFilter(categories: self.categories[safe: self.selectedCategories ?? -1]?.id, sortedBy: self.sortByArray[safe: self.selectedSortBy ?? -1], minPrice: minPrice, maxPrice: maxPrice)
            } else {
                self.delegate?.didFilter(services: self.services[safe: self.selectedService ?? -1]?.id, sortedBy: self.sortByArray[safe: self.selectedSortBy ?? -1], minPrice: minPrice, maxPrice: maxPrice)
            }
        }
    }
}
extension FilterPlacesController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == sortByCollection {
            return CGSize(width: (sortByArray[indexPath.row].localized.widthWithConstrainedWidth(width: collectionView.width)) + 10, height: 35)
        } else {
            if categoryID == nil {
                return CGSize(width: (categories[indexPath.row].name?.widthWithConstrainedWidth(width: collectionView.width) ?? 0) + 10, height: 35)
            } else {
                return CGSize(width: (services[indexPath.row].name?.widthWithConstrainedWidth(width: collectionView.width) ?? 0) + 10, height: 35)
            }
        }
       
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == sortByCollection {
            return 3
        } else {
            if categoryID == nil {
                return categories.count
            } else {
                return services.count
            }
        }
       
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.cell(type: ResturantFoodTypeCell.self, indexPath)
        
        if collectionView == sortByCollection {
            cell.titleLbl.text = sortByArray[indexPath.row].localized
            if  selectedSortBy == indexPath.row {
                cell.backView.backgroundColor = .appColor
                cell.titleLbl.textColor = .white
            } else {
                cell.backView.backgroundColor = UIColor(red: 234/255, green: 234/255, blue: 234/255, alpha: 1)
                cell.titleLbl.textColor = .black
            }
        } else {
            if categoryID == nil {
                cell.titleLbl.text = categories[indexPath.row].name
                //            if selectedCategories.contains(categories[indexPath.row].id ?? 0) {
                //                cell.backView.backgroundColor = .appColor
                //                cell.titleLbl.textColor = .white
                //            } else {
                //                cell.backView.backgroundColor = UIColor(red: 234/255, green: 234/255, blue: 234/255, alpha: 1)
                //                cell.titleLbl.textColor = .black
                //            }
                if  selectedCategories == indexPath.row {
                    cell.backView.backgroundColor = .appColor
                    cell.titleLbl.textColor = .white
                } else {
                    cell.backView.backgroundColor = UIColor(red: 234/255, green: 234/255, blue: 234/255, alpha: 1)
                    cell.titleLbl.textColor = .black
                }
                
            } else {
                cell.titleLbl.text = services[indexPath.row].name
                //            if selectedService.contains(services[indexPath.row].id ?? 0) {
                //                cell.backView.backgroundColor = .appColor
                //                cell.titleLbl.textColor = .white
                //            } else {
                //                cell.backView.backgroundColor = UIColor(red: 234/255, green: 234/255, blue: 234/255, alpha: 1)
                //                cell.titleLbl.textColor = .black
                //            }
                if  selectedService == indexPath.row {
                    cell.backView.backgroundColor = .appColor
                    cell.titleLbl.textColor = .white
                } else {
                    cell.backView.backgroundColor = UIColor(red: 234/255, green: 234/255, blue: 234/255, alpha: 1)
                    cell.titleLbl.textColor = .black
                }
            }
        }
     
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if categoryID == nil {
//            if selectedCategories.contains(categories[indexPath.row].id ?? 0) {
//                selectedCategories.removeAll(categories[indexPath.row].id ?? 0)
//
//            } else {
//                selectedCategories.append(categories[indexPath.row].id ?? 0)
//            }
//        } else {
//            if selectedService.contains(services[indexPath.row].id ?? 0) {
//                selectedService.removeAll(services[indexPath.row].id ?? 0)
//
//            } else {
//                selectedService.append(services[indexPath.row].id ?? 0)
//            }
//        }
        
        if collectionView == sortByCollection {
            selectedSortBy = indexPath.row
        } else {
            if categoryID == nil {
                selectedCategories = indexPath.row
            } else {
                selectedService = indexPath.row
            }
        }
     

        collectionView.reloadData()
    }
}
