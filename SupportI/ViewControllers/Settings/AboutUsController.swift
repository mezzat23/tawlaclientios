//
//  Aboutus.swift
//  SupportI
//
//  Created by Adam on 4/14/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class AboutUsController: BaseController {
    @IBOutlet weak var pageTitleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var websiteLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    
    var settingModel: SettingModel?
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        setup()
        fetchSetting()
        fetchPage()
        // Do any additional setup after loading the view.
    }
    func setup() {
        descLbl.text = ""
        phoneLbl.text = settingModel?.data?.phone
        websiteLbl.text = settingModel?.data?.website
        emailLbl.text = settingModel?.data?.email
    }
    func fetchSetting() {
        startLoading()
        ApiManager.instance.connection(.settings, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(SettingModel.self, from: response ?? Data())
            self.settingModel = data
            self.setup()
        }
    }
    func fetchPage() {
        startLoading()
        let method = api(.pages, [9])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(PageModel.self, from: response ?? Data())
            self.descLbl.text = data?.data?.dataDescription?.htmlToString
        }
    }
    @IBAction func sendMessage(_ sender: Any) {
        let scene = self.pushViewController(TechnicalSupportController.self, storyboard: .setting)
        self.push(scene)
    }
}
