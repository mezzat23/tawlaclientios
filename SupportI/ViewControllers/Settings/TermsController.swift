//
//  Terms.swift
//  SupportI
//
//  Created by Adam on 3/26/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class TermsController: BaseController {
    enum PageType {
        case terms
        case question
        case support
        case join
        case policyReservation
        case policyReturn
        case policyUsage
        case contactUS
    }
    @IBOutlet weak var pageTitleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    
    var type: PageType?
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        fetchPage()
        setup()
        // Do any additional setup after loading the view.
    }
 
    func setup() {
        descLbl.text = ""
        switch type {
            case .terms:
                pageTitleLbl.text = "Terms and conditions".localized
            case .question:
                pageTitleLbl.text = "Common questions".localized
            case .support:
                pageTitleLbl.text = "Technical support".localized
            case .contactUS:
                pageTitleLbl.text = "Contact us".localized
            case .join:
                pageTitleLbl.text = "Join us".localized
            case .policyReservation:
                pageTitleLbl.text = "Reservation policy".localized
            case .policyReturn:
                pageTitleLbl.text = "Return policy".localized
            case .policyUsage:
                pageTitleLbl.text = "Usage policy".localized
            default:
                break
        }
    }
    func fetchPage() {
        startLoading()
        var method = ""
        switch type {
            case .terms:
                method = api(.pages, [7])
            case .question:
                method = api(.pages, [8])
            case .support:
                method = api(.pages, [14])
            case .contactUS:
                method = api(.pages, [14])
            case .join:
                method = api(.pages, [13])
            case .policyReservation:
                method = api(.pages, [10])
            case .policyReturn:
                method = api(.pages, [11])
            case .policyUsage:
                method = api(.pages, [12])
            default:
            break
        }
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(PageModel.self, from: response ?? Data())
            self.descLbl.text = data?.data?.dataDescription?.htmlToString
        }
    }
}
