//
//  Technicalsupport.swift
//  SupportI
//
//  Created by Adam on 3/26/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class TechnicalSupportController: BaseController {

    var isLoaded: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isLoaded {
            self.navigationController?.pop()
        } else {
            isLoaded = true
        }
    }
    func setup() {
        // Create a user object
        let user = FreshchatUser.sharedInstance();
        // To set an identifiable first name for the user
        user.firstName = UserRoot.fetch()?.data?.name
        // To set an identifiable last name for the user
        user.lastName = ""
        //To set user's email id
        user.email = UserRoot.fetch()?.data?.email
        //To set user's phone number
        user.phoneCountryCode = ""
        user.phoneNumber = UserRoot.fetch()?.data?.mobile
        Freshchat.sharedInstance().setUser(user)
        
        //You can set custom user properties for a particular user
       // Freshchat.sharedInstance().setUserPropertyforKey("customerType", withValue: "Premium")
        //You can set user demographic information
        //Freshchat.sharedInstance().setUserPropertyforKey("city", withValue: "San Bruno")
        //You can segment based on where the user is in their journey of using your app
        Freshchat.sharedInstance().setUserPropertyforKey("loggedIn", withValue: "true")
        //You can capture a state of the user that includes what the user has done in your app
        //Freshchat.sharedInstance().setUserPropertyforKey("transactionCount", withValue: "3")
        
        Freshchat.sharedInstance().showConversations(self)
    }

}
