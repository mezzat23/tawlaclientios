//
//  Map.swift
//  SupportI
//
//  Created by Adam on 3/18/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import GoogleMaps
class MapController: BaseController {
    
    @IBOutlet weak var currentLocationView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var searchTxf: UITextField! {
        didSet {
            searchTxf.delegate = self
        }
    }
    @IBOutlet weak var resturantCollection: UICollectionView! {
        didSet {
            resturantCollection.delegate = self
            resturantCollection.dataSource = self
        }
    }
    
    var mapHelper: GoogleMapHelper?
    var locationHelper: LocationHelper?
    var resturants: [AllResturantModel.Datum] = []
    var paramters: [String: Any] = [:]
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        fetchPlaces()
    }
    func setup() {
        locationHelper = .init()
        mapHelper = .init()
        mapHelper?.mapView = mapView
        mapHelper?.delegate = self
        mapHelper?.markerDataSource = self
        
        locationHelper?.onUpdateLocation = { [weak self] degree in
            self?.mapHelper?.updateCamera(lat: degree?.latitude ?? 0, lng: degree?.longitude ?? 0)
        }
        locationHelper?.currentLocation()
    }
}
// MARK:  Network
extension MapController {
    func fetchPlaces() {
        startLoading()
        self.resturants.removeAll()
        ApiManager.instance.paramaters = paramters
        ApiManager.instance.connection(.places, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(AllResturantModel.self, from: response ?? Data())
            self.resturants.append(contentsOf: data?.data ?? [])
            self.resturantCollection.reloadData()
            self.mapHelper?.refresh()
        }
    }
}
// MARK:  Map functions
extension MapController: GoogleMapHelperDelegate, MarkerDataSource {
    func marker() -> MarkerAttrbuite {
        var attr = MarkerAttrbuite()
        //attr.icon = #imageLiteral(resourceName: <#T##String#>)
        return attr
    }
    func setMarkers() -> [GMSMarker] {
        var markers: [GMSMarker] = []
        for rest in resturants {
            let model = GMSMarker.init(position: CLLocationCoordinate2D(latitude: rest.latitude?.double() ?? 0, longitude: rest.longitude?.double() ?? 0))
            model.userData = rest
            markers.append(model)
        }
        return markers
    }
    func didTapOnMyLocation() {
        
    }
    func didTapOnMyLocation(lat: Double, lng: Double) {
        
    }
    func didChangeCameraLocation(lat: Double, lng: Double) {
        paramters["near_by_location"] = "\(lat),\(lng)"
        fetchPlaces()
    }
    func didTapOnMarker(marker: GMSMarker) {
        let rest = marker.userData as? AllResturantModel.Datum
        let scene = controller(ResturantDetailsController.self, storyboard: .resturant)
        scene.resturantID = rest?.id
        self.push(scene)
    }
}
extension MapController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        paramters["name"] = textField.text
        fetchPlaces()
    }
}
// MARK:  collection view
extension MapController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return resturants.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = collectionView.cell(type: Locationhome.self, indexPath)
        cell.closureFavorite = { row in
            self.addToFavorite(id: self.resturants[indexPath.row].id)
        }
        cell.model = resturants[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 240, height: 240)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let scene = controller(ResturantDetailsController.self, storyboard: .resturant)
        scene.resturantID = resturants[indexPath.row].id
        self.push(scene)
    }
}
extension MapController {
    func addToFavorite(id: Int?) {
        startLoading()
        let method = api(.places, [id ?? 0, "favorites"])
        ApiManager.instance.connection(method, type: .post) { (response) in
            self.stopLoading()
            self.makeWithoutCancelAlert("Success operation".localized, closure: {})
        }
    }
}
