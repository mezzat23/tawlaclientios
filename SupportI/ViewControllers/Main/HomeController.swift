//
//  Home.swift
//  SupportI
//
//  Created by Adam on 3/18/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class HomeController: BaseController {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var welcomeLbl: UILabel!
    //@IBOutlet weak var calenderLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var bookingView: UIView!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var offersView: Myview!
    @IBOutlet weak var imagesCollection: UICollectionView!
    @IBOutlet weak var categoriesCollection: UICollectionView!
    @IBOutlet weak var suggestionCollection: UICollectionView!
    @IBOutlet weak var offersCollection: UICollectionView!
    @IBOutlet weak var offersCategoryCollection: UICollectionView!
    @IBOutlet weak var offersCategoryHeight: NSLayoutConstraint!
    @IBOutlet weak var offerLbl: UILabel!
    @IBOutlet weak var offerDiscountLbl: UILabel!
    
    var model: HomeModel?
    var featuredOffers: [FeaturedOffer.Datum] = []
    var cities: [CityModel.Datum] = []
    var sliders: [SliderModel.Datum] = []
    var cityID: Int?
    var cityPath: Int?
    var user: User? {
        if UserRoot.fetch()?.data?.verified == 0 {
            return nil
        }
        return UserRoot.fetch()?.data
    }
    static var LOGIN_ALERT: Bool = false
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        fetchCities()
        fetchSliders()
        handlers()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        didBook()
        if HomeController.LOGIN_ALERT == true {
            checkLogin()
            HomeController.LOGIN_ALERT = false
            return
        }
    }
    func checkLogin() {
        if self.user == nil {
            let scene = self.controller(LoginController.self, storyboard: .pop)
            scene.delegate = self
            self.pushPop(vcr: scene)
        } else if UserRoot.fetch()?.token != nil {
            if UserRoot.fetch()?.data?.verified == 0 {
                let scene = self.controller(LoginController.self, storyboard: .pop)
                scene.delegate = self
                self.pushPop(vcr: scene)
                return
            } else {
                let scene = self.controller(ProfileController.self, storyboard: .auth)
                self.push(scene)
            }
        }
    }
    func setup() {
        if user != nil {
            userImage.isHidden = false
            userImage.setImage(url: user?.image)
            welcomeLbl.text = "\("Welcome".localized), \(user?.name ?? "")"
        } else {
            userImage.isHidden = true
            welcomeLbl.text = "\("Welcome".localized), \("login".localized)"
        }
        
        //calenderLbl.text = ""
        cityLbl.text = "City".localized
        imagesCollection.delegate = self
        imagesCollection.dataSource = self
        categoriesCollection.delegate = self
        categoriesCollection.dataSource = self
        suggestionCollection.delegate = self
        suggestionCollection.dataSource = self
        offersCollection.delegate = self
        offersCollection.dataSource = self
        offersCategoryCollection.delegate = self
        offersCategoryCollection.dataSource = self
    }
    func handlers() {
        offersView.UIViewAction { [weak self] in
            
        }
        cityView.UIViewAction { [weak self] in
            guard let scene = self?.controller(SelectCityController.self, storyboard: .pop) else { return }
            scene.source = self?.cities ?? []
            scene.selectedCity = self?.cityPath
            scene.onSelectCity = { path in
                self?.refreshCity(path: path)
            }
            self?.pushPop(vcr: scene)
        }
        bookingView.UIViewAction { [weak self] in
//            guard let scene = self?.controller(BookingPopUpController.self, storyboard: .pop) else { return }
//            scene.delegate = self
//            self?.pushPop(vcr: scene)
        }
        userImage.UIViewAction { [weak self] in
            guard let scene = self?.controller(ProfileController.self, storyboard: .auth) else { return }
            self?.push(scene)
        }
        welcomeLbl.UIViewAction {
            if self.user == nil {
                let scene = self.controller(LoginController.self, storyboard: .pop)
                scene.delegate = self
                self.pushPop(vcr: scene)
            } else if UserRoot.fetch()?.token != nil {
                if UserRoot.fetch()?.data?.verified == 0 {
                    let scene = self.controller(LoginController.self, storyboard: .pop)
                    scene.delegate = self
                    self.pushPop(vcr: scene)
                    return
                } else {
                    let scene = self.controller(ProfileController.self, storyboard: .auth)
                    self.push(scene)
                }
            }
        }
    }
 
  
}
// MARK:  Network
extension HomeController {
    func fetchHome() {
        startLoading()
        if cityID != nil {
            ApiManager.instance.paramaters["cityId"] = cityID
        }
        ApiManager.instance.connection(.home, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(HomeModel.self, from: response ?? Data())
            self.model = data
            self.refresh()
        }
    }
    func fetchSliders() {
        ApiManager.instance.connection(.sliders, type: .get) { (response) in
            let data = try? JSONDecoder().decode(SliderModel.self, from: response ?? Data())
            self.sliders.append(contentsOf: data?.data ?? [])
            self.imagesCollection.reloadData()
        }
    }
//    func fetchFeatured() {
//        startLoading()
//        ApiManager.instance.connection(.featuredOffers, type: .get) { (response) in
//            self.stopLoading()
//            let data = try? JSONDecoder().decode(FeaturedOffer.self, from: response ?? Data())
//            self.featuredOffers.append(contentsOf: data?.data ?? [])
//            self.imagesCollection.reloadData()
//        }
//    }
    func fetchCities() {
        ApiManager.instance.connection(.cities, type: .get) { (response) in
            let data = try? JSONDecoder().decode(CityModel.self, from: response ?? Data())
            self.cities.append(contentsOf: data?.data?.data ?? [])
            //self.refreshCity()
            self.fetchHome()
        }
    }
    func refreshCity(path: Int = 0) {
        self.cityLbl.text = self.cities[safe: path]?.name
        self.cityPath = path
        self.cityID = self.cities[safe: path]?.id
        self.fetchHome()
    }
    func refresh() {
        offerLbl.text = model?.offers?.first?.name
        offerDiscountLbl.text = "\(model?.offers?.first?.price ?? "") %"
        categoriesCollection.reloadData()
        suggestionCollection.reloadData()
        offersCollection.reloadData()
        offersCategoryCollection.reloadData()
    }
}
// MARK:-  Actions
extension HomeController {
    @IBAction func sugesstionPlacesAction(_ sender: Any) {
    }
    @IBAction func categoriesAction(_ sender: Any) {
        let scene = controller(ResturantsController.self, storyboard: .main)
        scene.forService = true
        scene.categoryTitle = "See all categories"
        self.push(scene)
    }
    @IBAction func qrCode(_ sender: Any) {
        let scene = controller(QrcodeController.self, storyboard: .pop)
        scene.delegate = self
        pushPop(vcr: scene)
    }
    @IBAction func lang(_ sender: Any) {
        changeLang {
            let controller = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
            guard let nav = controller else { return }
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                Localizer.initLang()
                let delegate = UIApplication.shared.delegate as? AppDelegate
                delegate?.window?.rootViewController = nav
            }
        }
    }
}
// MARK:  collection view
extension HomeController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
            case imagesCollection:
                return sliders.count
            case categoriesCollection:
                return model?.placeTypes?.count ?? 0
            case suggestionCollection:
                return model?.bestPlace?.count ?? 0
            case offersCollection:
                return model?.offers?.count ?? 0
            case offersCategoryCollection:
                return model?.serviceTypes?.count ?? 0
            default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
            case imagesCollection:
            var cell = collectionView.cell(type: Imagehomeslider.self, indexPath)
            cell.model = sliders[indexPath.row].image
            return cell
            case categoriesCollection:
                var cell = collectionView.cell(type: Catogaryhome.self, indexPath)
                cell.model = model?.placeTypes?[indexPath.row]
                return cell
            case suggestionCollection:
                var cell = collectionView.cell(type: Locationhome.self, indexPath)
                cell.closureFavorite = { row in
                    self.addToFavorite(id: self.model?.bestPlace?[indexPath.row].id)
                }
                cell.model = model?.bestPlace?[indexPath.row]
                return cell
            case offersCollection:
                var cell = collectionView.cell(type: Offerhome.self, indexPath)
                cell.model = model?.offers?[indexPath.row]
                return cell
            case offersCategoryCollection:
                var cell = collectionView.cell(type: Offertypehome.self, indexPath)
                cell.model = model?.serviceTypes?[indexPath.row]
                return cell
            default:
                return UICollectionViewCell()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
            case imagesCollection:
                return CGSize(width: collectionView.width - 40, height: collectionView.height)
            case categoriesCollection:
                return CGSize(width: 92, height: 128)
            case suggestionCollection:
                return CGSize(width: 240, height: 288)
            case offersCollection:
                return CGSize(width: 240, height: 320)
            case offersCategoryCollection:
                return CGSize(width: 126, height: 150)
            default:
                return CGSize(width: 0, height: 0)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
            case imagesCollection:
                self.displayImage(image: sliders[indexPath.row].image)
            case categoriesCollection:
                let scene = controller(ResturantsController.self, storyboard: .main)
                scene.categoryID = model?.placeTypes?[indexPath.row].id
                scene.categoryTitle = model?.placeTypes?[indexPath.row].name
                self.push(scene)
            case suggestionCollection:
                let scene = controller(ResturantDetailsController.self, storyboard: .resturant)
                scene.resturantID = model?.bestPlace?[indexPath.row].id
                self.push(scene)
            case offersCollection:
            break
            case offersCategoryCollection:
                let scene = controller(ResturantsController.self, storyboard: .main)
                scene.serviceType = model?.serviceTypes?[indexPath.row].id
                scene.categoryTitle = model?.serviceTypes?[indexPath.row].name
                self.push(scene)
            break
            default:
            break
        }
    }
}
// MARK:  Image Popup
extension HomeController: ImageDisplayInterface {
    
}
extension HomeController: BookingPopUpDelegate {
    func didBook() {
        //calenderLbl.text = "\("Reserve".localized) \("for".localized) \((BookModel.instance().persons ?? 0)+(BookModel.instance().childs ?? 0)) . \(BookModel.instance().date ?? DateHelper().currentDate() ?? "")"
    }
}
extension HomeController: QrCodeDelegate {
    func didFindTable(for table: ReservationsModel.Datum?) {
        let scene = controller(TableQRScanController.self, storyboard: .book)
        scene.reservation = table
        self.push(scene)
    }
    
    func didFindPlace(for place: String?) {
        let scene = controller(ResturantDetailsController.self, storyboard: .resturant)
        scene.resturantID = place?.int
        scene.QRCODE = true
        self.push(scene)
    }
}
extension HomeController {
    func addToFavorite(id: Int?) {
        startLoading()
        let method = api(.places, [id ?? 0, "favorites"])
        ApiManager.instance.connection(method, type: .post) { (response) in
            self.stopLoading()
            self.makeWithoutCancelAlert("Success operation".localized, closure: {})
        }
    }
}
// MARK:  Login delegate
extension HomeController: LoginDelegate, RegisterDelegate {
    func didOpenRegister() {
        let scene = controller(RegisterController.self, storyboard: .pop)
        scene.delegate = self
        pushPop(vcr: scene)
    }
    func didLogin() {
        if UserRoot.fetch()?.data?.verified == 0 {
            let scene = controller(VerfiycodeController.self, storyboard: .pop)
            scene.delegate = self
            scene.prev = true
            scene.closurePrevVC = {
                let scene = self.controller(LoginController.self, storyboard: .pop)
                scene.delegate = self
                self.pushPop(vcr: scene)
            }
            pushPop(vcr: scene)
        } else {
            setup()
        }
    }
    func didVerify() {
        setup()
    }
    func didRegister() {
        if UserRoot.fetch()?.data?.verified == 0 {
            let scene = controller(VerfiycodeController.self, storyboard: .pop)
            scene.delegate = self
            scene.prev = true
            scene.closurePrevVC = {
                let scene = self.controller(RegisterController.self, storyboard: .pop)
                scene.delegate = self
                self.pushPop(vcr: scene)
            }
            pushPop(vcr: scene)
        }
    }
}
