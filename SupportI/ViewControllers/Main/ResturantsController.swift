//
//  Resurants.swift
//  SupportI
//
//  Created by Adam on 3/19/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class ResturantsController: BaseController {
    @IBOutlet weak var filtersHeight: NSLayoutConstraint!
    @IBOutlet weak var filtersView: UIView!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var calenderView: UIView!
    @IBOutlet weak var calenderLbl: UILabel!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var favoriteBtn: RadioButton!
    @IBOutlet weak var offersBtn: RadioButton!
    @IBOutlet weak var filterLbl: UILabel!
    @IBOutlet weak var offersConstraint: NSLayoutConstraint!
    @IBOutlet weak var resturantTbl: UITableView! {
        didSet {
            resturantTbl.delegate = self
            resturantTbl.dataSource = self
        }
    }
    var paramters: [String: Any] = [:]
    var categoryID: Int?
    var categoryTitle: String?
    var resturants: [AllResturantModel.Datum] = []
    var cities: [CityModel.Datum] = []
    var cityID: Int?
    var cityPath: Int?
    var serviceType: Int?
    var forService: Bool = false
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        setup()
        didBook()
        handlers()
        fetchCities()
        // Do any additional setup after loading the view.
    }
    func setup() {
        if serviceType != nil || forService {
            filtersView.isHidden = true
            filtersHeight.constant = 0
        }
        if Localizer.current == "ar" {
            offersConstraint = offersConstraint.checkMultiplayer()
        }
        categoryLbl.text = categoryTitle
        calenderLbl.text = ""
        cityLbl.text = "City".localized
    }
    func handlers() {
        cityView.UIViewAction { [weak self] in
            guard let scene = self?.controller(SelectCityController.self, storyboard: .pop) else { return }
            scene.source = self?.cities ?? []
            scene.selectedCity = self?.cityPath
            scene.onSelectCity = { path in
                self?.refreshCity(path: path)
            }
            self?.pushPop(vcr: scene)
        }
        calenderView.UIViewAction { [weak self] in
//            guard let scene = self?.controller(BookingPopUpController.self, storyboard: .pop) else { return }
//            scene.delegate = self
//            self?.pushPop(vcr: scene)
        }
        offersBtn.onSelect {
            self.paramters["offers"] = 1
            self.resturants.removeAll()
            self.fetchPlaces()
        }
        favoriteBtn.onSelect {
            self.paramters["fav"] = 1
            self.resturants.removeAll()
            self.fetchPlaces()
        }
        favoriteBtn.onDeselect {
            self.paramters["fav"] = nil
            self.resturants.removeAll()
            self.fetchPlaces()
        }
        offersBtn.onDeselect {
            self.paramters["offers"] = nil
            self.resturants.removeAll()
            self.fetchPlaces()
        }
        filterLbl.UIViewAction {
            let scene = self.controller(FilterPlacesController.self, storyboard: .pop)
            scene.categoryID = self.categoryID
            scene.delegate = self
            self.pushPop(vcr: scene)
        }
    }
    @IBAction func search(_ sender: Any) {
        self.tabBarController?.selectedIndex = 2
        self.navigationController?.popViewController()
    }
}
// MARK:  Network
extension ResturantsController {
    func fetchCities() {
        ApiManager.instance.connection(.cities, type: .get) { (response) in
            let data = try? JSONDecoder().decode(CityModel.self, from: response ?? Data())
            self.cities.append(contentsOf: data?.data?.data ?? [])
            self.fetchPlaces()
        }
    }
    func fetchPlaces() {
        startLoading()
        if serviceType == nil {
            paramters["place_type_id"] = categoryID
        } else {
            paramters["service_types"] = serviceType

        }
        ApiManager.instance.paramaters = paramters
        ApiManager.instance.connection(.places, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(AllResturantModel.self, from: response ?? Data())
            self.resturants.append(contentsOf: data?.data ?? [])
            self.resturantTbl.reloadData()
        }
    }
    func refreshCity(path: Int = 0) {
        self.resturants.removeAll()
        self.cityLbl.text = self.cities[safe: path]?.name
        self.cityPath = path
        self.cityID = self.cities[safe: path]?.id
        paramters["city_id"] = self.cities[safe: path]?.id
        self.fetchPlaces()
    }
}
// MARK:  Table View
extension ResturantsController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resturants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: Resturants.self, indexPath)
        cell.model = resturants[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let scene = controller(ResturantDetailsController.self, storyboard: .resturant)
        scene.resturantID = resturants[indexPath.row].id
        self.push(scene)
    }
}
extension ResturantsController: BookingPopUpDelegate {
    func didBook() {
        calenderLbl.text = "\("Reserve".localized) \("for".localized) \((BookModel.instance().persons ?? 0)+(BookModel.instance().childs ?? 0)) . \(BookModel.instance().date ?? DateHelper().currentDate() ?? "")"
    }
}
extension ResturantsController: FilterDelegate {
    func didFilter(services: Int?, sortedBy: String?, minPrice: Int?, maxPrice: Int?) {
        paramters["service_types"] = services
        if sortedBy == "Most visited" {
            paramters["international"] = nil
            paramters["is_featured"] = nil
            paramters["most_visited"] = 1
        } else if sortedBy == "Famous places" {
            paramters["international"] = nil
            paramters["is_featured"] = 1
            paramters["most_visited"] = nil
            
        } else if sortedBy == "Global places" {
            paramters["international"] = 1
            paramters["is_featured"] = nil
            paramters["most_visited"] = nil
        }
        if minPrice != nil && maxPrice != nil {
            paramters["price_range_from"] = minPrice
            paramters["price_range_to"] = maxPrice
        } else {
            paramters["price_range_from"] = nil
            paramters["price_range_to"] = nil
        }
        self.resturants.removeAll()
        self.fetchPlaces()
    }
    
    func didFilter(categories: Int?, sortedBy: String?, minPrice: Int?, maxPrice: Int?) {
        
    }
    
}
