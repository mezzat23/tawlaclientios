//
//  Booking.swift
//  SupportI
//
//  Created by Adam on 3/18/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class BookingController: BaseController {
    enum Tab {
        case current
        case prev
    }
    @IBOutlet weak var currentLbl: UILabel!
    @IBOutlet weak var prevLbl: UILabel!
    @IBOutlet weak var currentUnderline: UIView!
    @IBOutlet weak var prevUnderline: UIView!
    @IBOutlet weak var bookingTbl: UITableView!
    
    var tab: Tab = .current
    var reservations: [ReservationsModel.Datum] = []
    var selectedPath: Int?
    var user: User? {
        if UserRoot.fetch()?.data?.verified == 0 {
            return nil
        }
        return UserRoot.fetch()?.data
    }
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
       
        setup()
        handlers()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if user == nil {
            makeWithoutCancelAlert("Sorry, you must log in first to view reservations".localized) {
                guard let scene = UIStoryboard.init(name: "Main", bundle: nil).instantiateInitialViewController() else { return }
                HomeController.LOGIN_ALERT = true
                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                appDelegate?.window?.rootViewController = scene
                return
            }
            return
        }
        self.reservations.removeAll()
        fetchReservations()

    }
    func setup() {
        bookingTbl.delegate = self
        bookingTbl.dataSource = self
    }
    func handlers() {
        currentLbl.UIViewAction {
            self.tab = .current
            self.currentLbl.textColor = .appColor
            self.prevLbl.textColor = .black
            self.currentUnderline.backgroundColor = .appColor
            self.prevUnderline.backgroundColor = .clear
            self.reservations.removeAll()
            self.fetchReservations()
        }
        prevLbl.UIViewAction {
            self.tab = .prev
            self.currentLbl.textColor = .black
            self.prevLbl.textColor = .appColor
            self.currentUnderline.backgroundColor = .clear
            self.prevUnderline.backgroundColor = .appColor
            self.reservations.removeAll()
            self.bookingTbl.reloadData()
            self.fetchReservations()
        }
    }
    func reloadView() {
        if reservations.count == 0 {
            bookingTbl.isHidden = true
        } else {
            bookingTbl.isHidden = false
            bookingTbl.reloadData()
        }
    }
    func fetchReservations() {
        if tab == .current {
            ApiManager.instance.paramaters["due_date"] = "current"
        } else {
            ApiManager.instance.paramaters["due_date"] = "finished"
        }
        startLoading()
        ApiManager.instance.connection(.reservations, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(ReservationsModel.self, from: response ?? Data())
            self.reservations.append(contentsOf: data?.data ?? [])
            self.reloadView()
        }
    }

    @IBAction func explore(_ sender: Any) {
        self.tabBarController?.selectedIndex = 0
    }
}
extension BookingController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reservations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: ReservationCell.self, indexPath)
        if tab == .prev {
            cell.isPrev = true
        } else {
            cell.isPrev = false
        }
        cell.delegate = self
        cell.model = reservations[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let scene = controller(ReservationDetailController.self, storyboard: .reservation)
        scene.reservation = reservations[indexPath.row]
        if tab == .prev {
            scene.isPrev = true
        } else {
            scene.isPrev = false
        }
        push(scene)
    }
}
extension BookingController: ReservationCellDelegate {
    func didEdit(path: Int) {
        
    }
    
    func didCancel(path: Int) {
        let scene = controller(CancelOrderController.self, storyboard: .pop)
        scene.reservationID = reservations[path].id
        pushPop(vcr: scene)
    }
    
    func didRate(path: Int) {
        let scene = controller(RateReservationController.self, storyboard: .reservation)
        scene.reservation = reservations[path]
        push(scene)
    }
    
    func didReOrder(path: Int) {
        selectedPath = path
        var book = BookModel.instance()
        book.typeID = reservations[selectedPath ?? 0].gatheringType?.id
        book.save()
        let scene = controller(BookingPopUpController.self, storyboard: .pop)
        scene.delegate = self
        pushPop(vcr: scene)
    }
    
  
}

extension BookingController: BookingPopUpDelegate {
    func didBook() {
        let scene = controller(CompeleteReservationController.self, storyboard: .book)
        scene.placeID = reservations[selectedPath ?? 0].place?.id
        push(scene)
    }
}
