//
//  Foodmenu.swift
//  SupportI
//
//  Created by Adam on 3/19/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class ResturantMenuController: BaseController {
    @IBOutlet weak var categoriesCollection: UICollectionView! {
        didSet {
            categoriesCollection.delegate = self
            categoriesCollection.dataSource = self
        }
    }
    @IBOutlet weak var menuTbl: UITableView! {
        didSet {
            menuTbl.delegate = self
            menuTbl.dataSource = self
        }
    }
    @IBOutlet weak var completeBtn: UIButton!
    
    var resturantID: Int?
    var menues: [FoodMenuModel.Datum] = []
    var categories: [CategoryModel.Datum] = []
    var selectedCategory: Int?
    var paramters: [String: Any] = [:]
    var fromOrder: Bool = false
    var paramtersOrder: [String: Any] = [:]
    var table: TablesModel.Datum?
    var isQRCode: Bool = false
    var tableQRCode: ReservationsModel.Datum?
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        //categories.append(CategoryModel.Datum(id: nil, createdAt: nil, updatedAt: nil, name: "All".localized, translations: []))
        fetchCats()
        setup()
    }
    func setup() {
        if fromOrder {
            completeBtn.setTitle("Checkout".localized, for: .normal)
            completeBtn.isHidden = false
        } else {
            completeBtn.isHidden = true
        }
        categoriesCollection.reloadData()
        var list: [FoodMenuModel.Datum] = []
        for menu in menues {
            if menu.menuItems?.count ?? 0 > 0 {
                list.append(menu)
            }
        }
        menues.removeAll()
        menues.append(contentsOf: list)
    }
    func setupFilterCategory() {
        self.menues.removeAll()
        if selectedCategory == 0 {
            paramters["cat"] = nil
        } else {
            paramters["cat"] = categories[selectedCategory ?? 0].id
        }
        fetchMenu()
    }
    func setupParamters() -> [String: Any] {
        let bookModel = BookModel.instance()
        var paramters: [String: Any] = [:]
        if bookModel.menu?.count ?? 0 > 0 {
            var counter = 0
            for item in bookModel.menu ?? [] {
                paramters["product_id[\(counter)]"] = item.id
                paramters["quantity[\(counter)]"] = item.qty
                paramters["price[\(counter)]"] = item.price
                counter += 1
            }
        }
        return paramters
    }
    @IBAction func completeReservation(_ sender: Any) {
        if isQRCode {
            
            let scene = self.controller(BillDetailController.self, storyboard: .book)
            //scene.paramters = paramtersOrder
            scene.placeID = self.resturantID
            scene.isQRCode = self.isQRCode
            scene.tableQRCode = self.tableQRCode
            scene.table = self.table
            scene.updateFoodMenu = true
            self.push(scene)
            
//            startLoading()
//            var paramters: [String: Any] = [:]
//            paramters = setupParamters()
//            paramters["res_id"] = tableQRCode?.id
//            let method = api(.reservations, [resturantID ?? 0, "add"])
//            ApiManager.instance.paramaters = paramters
//            ApiManager.instance.connection(method, type: .post) { (response) in
//                BookModel.reset()
////                let scene = self.controller(BillDetailController.self, storyboard: .book)
////                scene.paramters = self.paramtersOrder
////                scene.placeID = self.resturantID
////                scene.isQRCode = self.isQRCode
////                scene.tableQRCode = self.tableQRCode
////                scene.table = self.table
////                self.push(scene)
//
//                let scene = self.controller(OrderCompletedController.self, storyboard: .reservation)
//                scene.reservtion = self.tableQRCode
//                self.push(scene)
//
//
//
//
//            }
        } else {
            let scene = self.controller(BillDetailController.self, storyboard: .book)
            scene.paramters = paramtersOrder
            scene.placeID = self.resturantID
            scene.isQRCode = self.isQRCode
            scene.tableQRCode = self.tableQRCode
            scene.table = self.table
            self.push(scene)
        }
      
    }
}
// MARK:  Network
extension ResturantMenuController {
    func fetchCats() {
        startLoading()
        let method = api(.places, [resturantID ?? 0, "cats"])
        ApiManager.instance.connection(method, type: .get) { (response) in
            let data = try? JSONDecoder().decode(CategoryModel.self, from: response ?? Data())
            self.categories.append(contentsOf: data?.data?.data ?? [])
            self.fetchMenu()
        }
    }
    func fetchMenu() {
        startLoading()
        let method = api(.places, [resturantID ?? 0, "place_menu"])
        ApiManager.instance.paramaters = paramters
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(FoodMenuModel.self, from: response ?? Data())
            self.menues.append(contentsOf: data?.data?.data ?? [])
            self.setup()
            self.menuTbl.reloadData()
            ViewModelCore().paginator(respnod: data?.data?.data)
        }
    }
}
// MARK:  Collection view
extension ResturantMenuController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView == menuTbl {
            menuTbl.swipeButtomRefresh {
                if ViewModelCore().runPaginator() {
                    self.fetchMenu()
                } else {
                    self.menuTbl.stopSwipeButtom()
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: categories[indexPath.row].name?.width ?? 0, height: 33)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.cell(type: ResturantFoodTypeCell.self, indexPath)
        cell.backView.alpha = 0.20
        cell.titleLbl.text = categories[indexPath.row].name
        cell.titleLbl.textColor = .white
        if selectedCategory == indexPath.row {
            cell.backView.borderColor = .appColor
            cell.backView.borderWidth = 1
            cell.titleLbl.textColor = .appColor
        } else {
            cell.backView.borderWidth = 0
            cell.titleLbl.textColor = .white
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedCategory == indexPath.row {
            return
        }
        selectedCategory = indexPath.row
        setup()
        setupFilterCategory()
    }
}
// MARK:  Table View
extension ResturantMenuController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: Foodmenuheader.self, indexPath)
        cell.fromOrder = fromOrder
        cell.model = menues[indexPath.row]
        return cell
    }
}
