//
//  Ratings.swift
//  SupportI
//
//  Created by Adam on 3/22/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import Cosmos
class ResturantRatingController: BaseController {
    @IBOutlet weak var totalRateLbl: UILabel!
    @IBOutlet weak var rate: CosmosView!
    @IBOutlet weak var rateNumLbl: UILabel!
    @IBOutlet weak var ratesTbl: UITableView!
    @IBOutlet weak var reservationRate: CosmosView!
    @IBOutlet weak var placeRate: CosmosView!
    @IBOutlet weak var eatRate: CosmosView!
    
    var rates: [AllResturantModel.Rate] = []
    var reviewModel: RatesModel?
    var resturantID: Int?
    var resturant: AllResturantModel.Datum?
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        fetchRates()
        setup()
    }
    func setup() {
        if Localizer.current == "ar" {
            totalRateLbl.text = "5/\(resturant?.rate?.double()?.int.string ?? "0")"
        } else {
            totalRateLbl.text = "\(resturant?.rate?.double()?.int.string ?? "0")/5"
        }
        rate.rating = resturant?.rate?.double() ?? 0
        rateNumLbl.text = "\("rates".localized): \(resturant?.ratesNum ?? 0)"
        placeRate.rating = reviewModel?.data2?.allPlaceRate?.int.double ?? 0
        reservationRate.rating = reviewModel?.data2?.allServiceRate?.int.double ?? 0
        eatRate.rating = reviewModel?.data2?.allFoodRate?.int.double ?? 0
        
        ratesTbl.delegate = self
        ratesTbl.dataSource = self
    }
    func refresh() {
        ratesTbl.reloadData()
    }
}
// MARK:  Network
extension ResturantRatingController {
    func fetchRates() {
        startLoading()
        let method = api(.places, [resturantID ?? 0, "rates"])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(RatesModel.self, from: response ?? Data())
            self.rates.append(contentsOf: data?.data ?? [])
            self.reviewModel = data
            self.refresh()
        }
    }
}
// MARK:  Table view
extension ResturantRatingController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: Comments.self, indexPath)
        cell.model = rates[indexPath.row]
        return cell
    }
}
