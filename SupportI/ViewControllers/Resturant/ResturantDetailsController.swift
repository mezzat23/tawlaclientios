//
//  Detailsresturant.swift
//  SupportI
//
//  Created by Adam on 3/22/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import Cosmos
import GoogleMaps
class ResturantDetailsController: BaseController {
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
        }
    }
    @IBOutlet weak var topViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollViewTop: NSLayoutConstraint!
    @IBOutlet weak var resturantImage: UIImageView!
    @IBOutlet weak var resturantPlaceLbl: UILabel!
    @IBOutlet weak var resturantNameLbl: UILabel!
    @IBOutlet weak var resturantRate: CosmosView!
    @IBOutlet weak var resturantRateLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var discountDescLbl: UILabel!
    @IBOutlet weak var reserveTimeLbl: UILabel!
    @IBOutlet weak var reserveFloorLbl: UILabel!
    @IBOutlet weak var placeInfoViewHeight: NSLayoutConstraint!
    @IBOutlet weak var foodTypeCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var resturantMobileLbl: UILabel!
    @IBOutlet weak var resturantAddressLbl: UILabel!
    @IBOutlet weak var resturantLocationMap: GMSMapView!
    @IBOutlet weak var foodTypeCollection: UICollectionView!
    @IBOutlet weak var paymentMechanismLbl: UILabel!
    @IBOutlet weak var imagesCollection: UICollectionView!
    @IBOutlet weak var visitorTotalRateLbl: UILabel!
    @IBOutlet weak var totalRate: CosmosView!
    @IBOutlet weak var totalRateLbl: UILabel!
    @IBOutlet weak var ratesTbl: UITableView!
    @IBOutlet weak var foodMenuTbl: UITableView!
    @IBOutlet weak var ratesTblHeight: NSLayoutConstraint!
    @IBOutlet weak var foodMenuHeight: NSLayoutConstraint!
    @IBOutlet weak var featuresHeight: NSLayoutConstraint!
    @IBOutlet weak var featuresTbl: UITableView!
    @IBOutlet weak var reservationRate: CosmosView!
    @IBOutlet weak var placeRate: CosmosView!
    @IBOutlet weak var eatRate: CosmosView!
    @IBOutlet weak var daysCollection: UICollectionView!
    @IBOutlet weak var fromTimeView: UIView!
    @IBOutlet weak var toTimeView: UIView!
    @IBOutlet weak var fromTimeLbl: UILabel!
    @IBOutlet weak var toTimeLbl: UILabel!
    
    var selectedDay: Int = 0
    var resturantID: Int?
    var resturant: AllResturantModel.Datum?
    var reviewModel: RatesModel?
    var foodTypeHeight: CGFloat = 55
    var placeViewHeight: CGFloat = 600
    var ratesHeight: CGFloat = 120
    var foodHeight: CGFloat = 170
    var mapHelper: GoogleMapHelper?
    var QRCODE: Bool = false
    var user: User? {
        if UserRoot.fetch()?.data?.verified == 0 {
            return nil
        }
        return UserRoot.fetch()?.data
    }
    

}
// MARK:  Fuctions
extension ResturantDetailsController {
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        fetchResturant()
        fetchRates()
        setup()
        handlers()
    }
    
    func setup() {
        mapHelper = .init()
        mapHelper?.mapView = resturantLocationMap
        foodTypeCollection.delegate = self
        foodTypeCollection.dataSource = self
        imagesCollection.delegate = self
        imagesCollection.dataSource = self
        ratesTbl.dataSource = self
        ratesTbl.delegate = self
        foodMenuTbl.delegate = self
        foodMenuTbl.dataSource = self
        featuresTbl.delegate = self
        featuresTbl.dataSource = self
        daysCollection.delegate = self
        daysCollection.dataSource = self
    }
    func handlers() {
        resturantMobileLbl.UIViewAction {
            call(text: self.resturant?.phones?.first)
        }
    }
    func setupUI() {
        resturantImage.setImage(url: resturant?.image)
        resturantPlaceLbl.text = "\(resturant?.city ?? "") - \(resturant?.placeType?.name ?? "")   . \(resturant?.priceNote ?? "")"
        resturantNameLbl.text = resturant?.name
        resturantRate.rating = resturant?.rate?.double() ?? 0
        resturantRateLbl.text = "\("rates".localized): \(resturant?.ratesNum ?? 0)"
        discountLbl.text = "\(resturant?.offers?.first?.price ?? "")"
        discountDescLbl.text = "\(resturant?.offers?.first?.name ?? "")"
        reserveTimeLbl.text = "\("Reserve".localized) \("for".localized) \((BookModel.instance().persons ?? 0)+(BookModel.instance().childs ?? 0)) . \(BookModel.instance().date ?? DateHelper().currentDate() ?? "")"
        reserveFloorLbl.text = ""
        resturantMobileLbl.text = ""
        var counter = 0
        resturant?.phones?.forEach({ (item) in
            if counter == 0 {
                self.resturantMobileLbl.text = "\(item)"
            } else {
                self.resturantMobileLbl.text = "\(self.resturantMobileLbl.text ?? "") , \(item)"
            }
            counter += 1
        })
        
        resturantAddressLbl.text = resturant?.address
        mapHelper?.updateCamera(lat: resturant?.latitude?.double() ?? 0, lng: resturant?.longitude?.double() ?? 0)
        paymentMechanismLbl.text = resturant?.paymentTypes
        foodTypeCollection.reloadData()
        imagesCollection.reloadData()
        ratesTbl.reloadData()
        foodMenuTbl.reloadData()
        featuresTbl.reloadData()
    }
    func setupDays() {
        daysCollection.reloadData()
        fromTimeLbl.text = resturant?.workingDays?.first?.startAt
        toTimeLbl.text = resturant?.workingDays?.first?.endAt
    }
    func setupHeights() {
        if resturant?.rates?.count ?? 0 > 0 {
            ratesTblHeight.constant = ratesHeight
        } else {
            ratesTblHeight.constant = 0
        }
        if resturant?.menuItems?.count ?? 0 > 1 {
            foodMenuHeight.constant = foodHeight
        } else if resturant?.menuItems?.count ?? 0 == 1 {
            foodMenuHeight.constant = 85
        } else {
            foodMenuHeight.constant = 0
        }
        featuresHeight.constant = featuresTbl.contentSize.height
    }
    func setupRate() {
        if Localizer.current == "ar" {
            visitorTotalRateLbl.text = "5/\(resturant?.rate?.double()?.int.string ?? "0")"
        } else {
            visitorTotalRateLbl.text = "\(resturant?.rate?.double()?.int.string ?? "0")/5"
        }
        totalRate.rating = resturant?.rate?.double() ?? 0
        totalRateLbl.text = "\("rates".localized): \(resturant?.ratesNum ?? 0)"
        
        placeRate.rating = reviewModel?.data2?.allPlaceRate?.int.double ?? 0
        reservationRate.rating = reviewModel?.data2?.allServiceRate?.int.double ?? 0
        eatRate.rating = reviewModel?.data2?.allFoodRate?.int.double ?? 0
    }
    func checkLogin() {
        if UserRoot.token() == nil {
            let scene = controller(LoginController.self, storyboard: .pop)
            scene.delegate = self
            pushPop(vcr: scene)
        } else {
            if UserRoot.fetch()?.data?.verified == 0 {
                let scene = controller(LoginController.self, storyboard: .pop)
                scene.delegate = self
                pushPop(vcr: scene)
                return
            }
        }
    }
    
}
// MARK:  Actions
extension ResturantDetailsController {
    @IBAction func lang(_ sender: Any) {
        changeLang {
            let controller = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
            guard let nav = controller else { return }
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                Localizer.initLang()
                let delegate = UIApplication.shared.delegate as? AppDelegate
                delegate?.window?.rootViewController = nav
            }
        }
    }
    @IBAction func share(_ sender: Any) {
        shareApp(items: [resturant?.name ?? "", resturant?.address ?? "", resturant?.details ?? ""])
    }
    @IBAction func favorite(_ sender: Any) {
        if user == nil {
            makeWithoutCancelAlert("Sorry, you must log in first to use favorites".localized) {
                self.checkLogin()
            }
            return
        }
        addToFavorite()
    }
    @IBAction func reserveTable(_ sender: Any) {
//        if UserRoot.token() == nil {
//            let scene = controller(LoginController.self, storyboard: .pop)
//            scene.delegate = self
//            pushPop(vcr: scene)
//        } else {
//            if UserRoot.fetch()?.data?.verified == 0 {
//                let scene = controller(LoginController.self, storyboard: .pop)
//                scene.delegate = self
//                pushPop(vcr: scene)
//                return
//            }
//            let scene = controller(BookingPopUpController.self, storyboard: .pop)
//            scene.delegate = self
//            scene.QRCODE = QRCODE
//            pushPop(vcr: scene)
//        }
        if self.resturant?.closed == 1 {
            return makeWithoutCancelAlert("The resturant is closed now".localized, closure: {})
        }
        checkBookModel()
        let scene = controller(BookingPopUpController.self, storyboard: .pop)
        scene.delegate = self
        scene.QRCODE = QRCODE
        scene.resturantID = resturant?.id
        pushPop(vcr: scene)
    }
    @IBAction func foodMenu(_ sender: Any) {
        let scene = controller(ResturantMenuController.self, storyboard: .resturant)
        scene.resturantID = resturantID
        push(scene)
    }
    @IBAction func showOnMap(_ sender: Any) {
        var navigate = NavigateMap()
        navigate.lat = resturant?.latitude?.double() ?? 0
        navigate.lng = resturant?.longitude?.double() ?? 0
        navigate.openMapForPlace(delegate: self)
    }
    @IBAction func showRates(_ sender: Any) {
        let scene = controller(ResturantRatingController.self, storyboard: .resturant)
        scene.resturant = resturant
        scene.resturantID = resturantID
        push(scene)
    }
}
// MARK:  Network
extension ResturantDetailsController {
    func fetchRates() {
        startLoading()
        let method = api(.places, [resturantID ?? 0, "rates"])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(RatesModel.self, from: response ?? Data())
            self.reviewModel = data
            self.setupRate()
        }
    }
    func fetchResturant() {
        startLoading()
        let method = api(.places, [resturantID ?? 0])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(ResturantModel.self, from: response ?? Data())
            self.resturant = data?.data
            self.setupUI()
            self.setupHeights()
            self.setupRate()
            self.setupDays()
        }
    }
    func addToFavorite() {
        startLoading()
        let method = api(.places, [resturantID ?? 0, "favorites"])
        ApiManager.instance.connection(method, type: .post) { (response) in
            self.stopLoading()
            self.makeWithoutCancelAlert("Success operation".localized, closure: {})
        }
    }
}
// MARK:  Map
extension ResturantDetailsController: GoogleMapHelperDelegate {
    func didChangeCameraLocation(lat: Double, lng: Double) {
        mapHelper?.setMarker(position: CLLocationCoordinate2D(latitude: resturant?.latitude?.double() ?? 0, longitude: resturant?.longitude?.double() ?? 0))
    }
}
// MARK:  SCrollview
extension ResturantDetailsController {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.scrollView.contentOffset.y > 500 {
            self.scrollViewTop.constant = 0
        } else {
            self.scrollViewTop.constant = -50
        }
    }
}

// MARK:  Collection View
extension ResturantDetailsController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func setupImageSizePerPath(for path: Int) -> CGSize {
        if path % 2 == 0 {
            return CGSize(width: 150, height: imagesCollection.height)
        } else {
            return CGSize(width: 90, height: imagesCollection.height)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == foodTypeCollection {
            return CGSize(width: resturant?.placeServiceType?[indexPath.row].name?.width ?? 0, height: 25)
        } else if collectionView == daysCollection {
            return CGSize(width: 80 , height: 30)
        } else {
            return setupImageSizePerPath(for: indexPath.row)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == foodTypeCollection {
            return resturant?.placeServiceType?.count ?? 0
        } else if collectionView == daysCollection {
            return resturant?.workingDays?.count ?? 0
        } else {
            return resturant?.album?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == foodTypeCollection {
            var cell = collectionView.cell(type: ResturantFoodTypeCell.self, indexPath)
            cell.model = resturant?.placeServiceType?[indexPath.row]
            return cell
        } else if collectionView == daysCollection {
            let cell = collectionView.cell(type: DayCollectionCell.self, indexPath)
            cell.day = resturant?.workingDays?[indexPath.row]
            if selectedDay == indexPath.row {
                cell.isChecked = true
            } else {
                cell.isChecked = false
            }
            cell.setup()
            return cell
        } else {
            var cell = collectionView.cell(type: Imagehomeslider.self, indexPath)
            cell.model = resturant?.album?[indexPath.row].link
            cell.sliderImage.UIViewAction {
                self.displayImage(image: self.resturant?.album?[indexPath.row].link)
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == daysCollection {
            selectedDay = indexPath.row
            //checker = true
            let day = resturant?.workingDays?[indexPath.row]
            fromTimeLbl.text = day?.startAt
            toTimeLbl.text = day?.endAt
            collectionView.reloadData()
        }
    }
}
// MARK:  Image Display interface
extension ResturantDetailsController: ImageDisplayInterface {
    
}
// MARK:  Table view
extension ResturantDetailsController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == ratesTbl {
            if resturant?.rates?.count ?? 0 > 1 {
                return 1
            } else {
                return resturant?.rates?.count ?? 0
            }
        } else if tableView == foodMenuTbl {
            if resturant?.menuItems?.count ?? 0 > 2 {
                return 2
            } else {
                return resturant?.menuItems?.count ?? 0
            }
        } else {
            return resturant?.placeFeatures?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == ratesTbl {
            var cell = tableView.cell(type: Comments.self, indexPath)
            cell.model = resturant?.rates?[indexPath.row]
            return cell
        } else if tableView == foodMenuTbl {
            var cell = tableView.cell(type: Foodmenufooter.self, indexPath)
            cell.model = resturant?.menuItems?[indexPath.row]
            return cell
        } else {
            var cell = tableView.cell(type: FeaturesTableCell.self, indexPath)
            cell.model = resturant?.placeFeatures?[indexPath.row]
            return cell
        }
    }
}
// MARK:  Login delegate
extension ResturantDetailsController: LoginDelegate, RegisterDelegate {
    func didOpenRegister() {
        let scene = controller(RegisterController.self, storyboard: .pop)
        scene.delegate = self
        pushPop(vcr: scene)
    }
    func didLogin() {
        if UserRoot.fetch()?.data?.verified == 0 {
            let scene = controller(VerfiycodeController.self, storyboard: .pop)
            scene.delegate = self
            scene.prev = true
            scene.closurePrevVC = {
                let scene = self.controller(LoginController.self, storyboard: .pop)
                scene.delegate = self
                self.pushPop(vcr: scene)
            }
            pushPop(vcr: scene)
        }
    }
    func didVerify() {
        
    }
    func didRegister() {
        if UserRoot.fetch()?.data?.verified == 0 {
            let scene = controller(VerfiycodeController.self, storyboard: .pop)
            scene.delegate = self
            scene.prev = true
            scene.closurePrevVC = {
                let scene = self.controller(RegisterController.self, storyboard: .pop)
                scene.delegate = self
                self.pushPop(vcr: scene)
            }
            pushPop(vcr: scene)
        }
    }
}
extension ResturantDetailsController: BookingPopUpDelegate {
    func checkBookModel() {
        let model = BookModel.instance()
        if model.resturantID != resturant?.id {
            let model = BookModel(isCover: false, persons: 0, childs: 0, typeID: nil, position: BookModel.Position.inDoor.rawValue, date: nil, QRCODE: false, resturantID: nil)
            model.save()
        }
    }
    func didBook() {
        reserveTimeLbl.text = "\("Reserve".localized) \("for".localized) \((BookModel.instance().persons ?? 0)+(BookModel.instance().childs ?? 0)) . \(BookModel.instance().date ?? DateHelper().currentDate() ?? "")"
        let scene = controller(CompeleteReservationController.self, storyboard: .book)
        scene.placeID = resturantID
        push(scene)
    }
}
