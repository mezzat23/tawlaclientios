//
//  Editprofile.swift
//  SupportI
//
//  Created by Adam on 3/24/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class EditProfileController: BaseController {

    @IBOutlet weak var mobileTxf: UITextField!
    @IBOutlet weak var dialCodeLbl: UILabel!
    @IBOutlet weak var flagImage: UIImageView!
    @IBOutlet weak var fullnameTxf: UITextField!
    @IBOutlet weak var mrsBtn: UIButton!
    @IBOutlet weak var msBtn: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var emailTxf: UITextField!
    @IBOutlet weak var changePasswordView: UIView!
    @IBOutlet weak var changePasswordlbl: UILabel!
    
    var user: User? {
        return UserRoot.fetch()?.data
    }
    var imagePicker: GalleryPickerHelper?
    var isMr: Bool = true
    var password: String?
    var imageURL: URL?
    var dialCode = "+2"
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        setup()
        handlers()
        // Do any additional setup after loading the view.
    }
    func setup() {
        mobileTxf.isUserInteractionEnabled = false
        mobileTxf.text = user?.mobile
        fullnameTxf.text = user?.name
        emailTxf.text = user?.email
    }
    func handlers() {
        changePasswordView.UIViewAction {
            let scene = self.pushViewController(ChangePasswordController.self, storyboard: .pop)
            scene.delegate = self
            self.pushPop(vcr: scene)
        }
        changePasswordlbl.UIViewAction {
            let scene = self.pushViewController(ChangePasswordController.self, storyboard: .pop)
            scene.delegate = self
            self.pushPop(vcr: scene)
        }
        msBtn.UIViewAction {
            self.mrsBtn.backgroundColor = .lightGray
            self.msBtn.backgroundColor = UIColor(named: "orange") ?? .orange
            self.isMr = true
        }
        mrsBtn.UIViewAction {
            self.msBtn.backgroundColor = .lightGray
            self.mrsBtn.backgroundColor = UIColor(named: "orange") ?? .orange
            self.isMr = false
        }
        profileImageView.UIViewAction { [weak self] in
            self?.imagePicker = .init()
            self?.imagePicker?.onPickImage = { image in
                self?.profileImage.image = image
            }
            self?.imagePicker?.onPickImageURL = { url in
                self?.imageURL = url
            }
            self?.imagePicker?.pick(in: self)
        }
    }
    @IBAction func save(_ sender: Any) {
        updateProfile()
    }
}
// MARK:  Network
extension EditProfileController {
    func updateProfile() {
        startLoading()
        ApiManager.instance.paramaters["mobile"] = mobileTxf.text
        ApiManager.instance.paramaters["name"] = fullnameTxf.text
        ApiManager.instance.paramaters["email"] = emailTxf.text
        if password != nil {
            ApiManager.instance.paramaters["password"] = password
        }
        let method = api(.updateProfile)
        ApiManager.instance.downloaderDelegate = self
        ApiManager.instance.uploadFile(method, type: .post, file: ["avatar": imageURL]) { (response) in
            self.stopLoading()
            self.password = nil
            let data = try? JSONDecoder().decode(UserRoot.self, from: response ?? Data())
            let oldUser = UserRoot.fetch()
            data?.token = oldUser?.token
            data?.save()
            self.makeWithoutCancelAlert("The data has been modified successfully.".localized, closure: {})
        }
    }
}
// MARK:  change password
extension EditProfileController: ChangePasswordDelegate, DownloaderDelegate {
    func didChangePassword(password: String) {
        self.password = password
        self.updateProfile()
    }
}

// MARK:  Country
extension EditProfileController {
    func handleCountry() {
        if Localizer.current == "en" {
            dialCodeLbl.text = "966 🇸🇦"
        } else {
            dialCodeLbl.text = "🇸🇦 966"
        }
        dialCodeLbl.UIViewAction {
            self.fetchCountries()
            
            //            let navController = UINavigationController(rootViewController: countries)
            //            self.present(navController, animated: true, completion: nil)
        }
        flagImage.UIViewAction {
            self.fetchCountries()
        }
    }
    func openPickerCountry(countries: [CountryModel.Datum]) {
        let scene = controller(PickerViewHelper.self, storyboard: .PickerViewHelper)
        scene.source = []
        scene.source.append(contentsOf: countries)
        scene.didSelectClosure = { row in
            self.dialCodeLbl.text = countries[row].code
            self.dialCode = countries[row].code ?? ""
            self.flagImage.setImage(url: countries[row].flag)
        }
        scene.titleClosure = { row in
            return countries[row].name
        }
        pushPop(vcr: scene)
    }
    func fetchCountries() {
        startLoading()
        ApiManager.instance.connection(.countries, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(CountryModel.self, from: response ?? Data())
            self.openPickerCountry(countries: data?.data ?? [])
        }
    }
}
