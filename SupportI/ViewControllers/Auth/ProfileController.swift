//
//  Profile.swift
//  SupportI
//
//  Created by Adam on 3/23/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class ProfileController: BaseController {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var memberFromLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var mobileLbl: UILabel!
    @IBOutlet weak var settingsView: UIView!
    @IBOutlet weak var favoritesView: UIView!
    @IBOutlet weak var walletView: UIView!
    @IBOutlet weak var accountView: UIView!
    @IBOutlet weak var contactUsLbl: UILabel!
    @IBOutlet weak var langLbl: UILabel!
    @IBOutlet weak var langTextLbl: UILabel!
    
    var user: User? {
        return UserRoot.fetch()?.data
    }
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        setup()
        handlers()
        // Do any additional setup after loading the view.
    }
    func setup() {
        userImage.setImage(url: user?.image)
        memberFromLbl.text = "\("Member since".localized) \(user?.createdAt ?? "")"
        usernameLbl.text = user?.name
        mobileLbl.text = user?.mobile
        if Localizer.current == "ar" {
            langLbl.text = "العربية"
        } else {
            langLbl.text = "English"
        }
    }
    func handlers() {
        settingsView.UIViewAction {
            let scene = self.pushViewController(SettingController.self, storyboard: .setting)
            self.push(scene)
        }
        favoritesView.UIViewAction {
            let scene = self.pushViewController(FavoraiteController.self, storyboard: .other)
            self.push(scene)
        }
        walletView.UIViewAction {
            let scene = self.pushViewController(Wallet.self, storyboard: .other)
            self.push(scene)
        }
        accountView.UIViewAction {
            let scene = self.pushViewController(EditProfileController.self, storyboard: .auth)
            self.push(scene)
        }
        contactUsLbl.UIViewAction {
            let scene = self.pushViewController(TechnicalSupportController.self, storyboard: .setting)
            self.push(scene)
        }
       
    }
    @IBAction func language(_ sender: Any) {
        changeLang {
            let controller = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
            guard let nav = controller else { return }
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                Localizer.initLang()
                let delegate = UIApplication.shared.delegate as? AppDelegate
                delegate?.window?.rootViewController = nav
            }
        }
    }
    @IBAction func logout(_ sender: Any) {
        makeAlert("Are you sure ?".localized) {
            UserRoot.save(response: Data())
            let controller = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
            guard let nav = controller else { return }
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                let delegate = UIApplication.shared.delegate as? AppDelegate
                delegate?.window?.rootViewController = nav
            }
        }
        
    }
    @IBAction func notifications(_ sender: Any) {
        let scene = pushViewController(NotificationsController.self, storyboard: .other)
        push(scene)
    }
    @IBAction func share(_ sender: Any) {
        shareApp(items: [user?.name ?? ""])
    }
}
