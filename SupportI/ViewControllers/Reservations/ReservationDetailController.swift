//
//  Detailsreservation.swift
//  SupportI
//
//  Created by Adam on 3/26/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class ReservationDetailController: BaseController {
    @IBOutlet weak var resturantImage: UIImageView!
    @IBOutlet weak var resturantNameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var typeCountLbl: UILabel!
    @IBOutlet weak var tableNumLbl: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    var reservation: ReservationsModel.Datum?
    var reservationID: Int?
    var isPrev: Bool = false
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setup()
        if reservationID != nil {
            fetchReservations()
        }
    }
    
    func fetchReservations() {
        startLoading()
        let method = api(.reservations, [reservationID ?? 0])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(ReservationSingleModel.self, from: response ?? Data())
            self.reservation = data?.data
            self.setup()
        }
    }
    
    func setup() {
        resturantImage.setImage(url: reservation?.place?.image)
        resturantNameLbl.text = reservation?.place?.name
        dateLbl.text = "\(reservation?.date ?? "") \(reservation?.startAt ?? "")"
        typeLbl.text = reservation?.gatheringType?.name
        typeCountLbl.text = "\((reservation?.adultCount ?? 0) + (reservation?.childrenCount ?? 0))"
        tableNumLbl.text = reservation?.table?.number?.string ?? "0"
        
        if isPrev {
            editBtn.isHidden = false
            if reservation?.rated == 1 {
                editBtn.isHidden = true
            }
            editBtn.setImage(#imageLiteral(resourceName: "star"), for: .normal)
            editBtn.setTitle("Rate Book".localized, for: .normal)
            
            cancelBtn.setImage(#imageLiteral(resourceName: "bookings_nav-1"), for: .normal)
            cancelBtn.setTitle("Reorder Book".localized, for: .normal)
        } else {
            editBtn.isHidden = true
            editBtn.setImage(#imageLiteral(resourceName: "booking_edit"), for: .normal)
            editBtn.setTitle("Edit Book".localized, for: .normal)
            
            if reservation?.status == "approved" || reservation?.status == "canceled" {
                cancelBtn.isHidden = true
            } else {
                cancelBtn.isHidden = false
            }
            cancelBtn.setImage(#imageLiteral(resourceName: "delete"), for: .normal)
            cancelBtn.setTitle("Cancel Book".localized, for: .normal)
        }
        
    }
    @IBAction func editBook(_ sender: Any) {
        if isPrev == true {
            let scene = controller(RateReservationController.self, storyboard: .reservation)
            scene.reservation = reservation
            scene.closureDetailReservation = {
                self.navigationController?.pop()
            }
            push(scene)
        } else {
            
        }
    }
    @IBAction func cancelBook(_ sender: Any) {
        let scene = controller(CancelOrderController.self, storyboard: .pop)
        scene.reservationID = reservation?.id
        pushPop(vcr: scene)
    }
    
    override func backBtn(_ sender: Any) {
        if reservationID != nil {
            for controller in self.navigationController?.viewControllers ?? [] {
                if controller is HomeController || controller is UITabBarController {
                    self.navigationController?.popToViewController(controller, animated: true)
                }
            }
        } else {
            self.navigationController?.popViewController()
        }
    }
    @IBAction func seeAllBooking(_ sender: Any) {
        if reservationID != nil {
            for controller in self.navigationController?.viewControllers ?? [] {
                if controller is HomeController || controller is UITabBarController {
                    self.navigationController?.popToViewController(controller, animated: true)
                    controller.tabBarController?.selectedIndex = 3
                }
            }
        } else {
            self.navigationController?.pop()
        }
    }
    
}
