//
//  Ordersucces.swift
//  SupportI
//
//  Created by Adam on 3/24/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import goSellSDK

class BillDetailController: BaseController {
    @IBOutlet weak var reserveIDLbl: UILabel!
    @IBOutlet weak var reserveDateLbl: UILabel!
    @IBOutlet weak var tableImage: UIImageView!
    @IBOutlet weak var tableCodeLbl: UILabel!
    @IBOutlet weak var tablePrice: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var productsTbl: UITableView!
    @IBOutlet weak var productsHeight: NSLayoutConstraint!
    @IBOutlet weak var walletView: UIView!
    @IBOutlet weak var walletLbl: UILabel!
    @IBOutlet weak var onlineView: UIView!
    @IBOutlet weak var couponTxf: UITextField!
    @IBOutlet weak var initialValueLbl: UILabel!
    @IBOutlet weak var couponLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var haveCouponLbl: UILabel!
    @IBOutlet weak var applyBtn: UIButton!
    @IBOutlet weak var couponImage: UIImageView!
    @IBOutlet weak var paymentHeight: NSLayoutConstraint!
    @IBOutlet weak var paymentMethodLbl: UILabel!
    @IBOutlet weak var topPaymentConstaraint: NSLayoutConstraint!
    @IBOutlet weak var topReservationBill: NSLayoutConstraint!
    @IBOutlet weak var qrCodeImage: UIImageView!
    @IBOutlet weak var reservationIDTitleLbl: UILabel!
    @IBOutlet weak var completeBtn: UIButton!
    @IBOutlet weak var reserveIDConstraint: NSLayoutConstraint!
    
    var paramters: [String: Any] = [:]
    var placeID: Int?
    var bookModel: BookModel?
    var table: TablesModel.Datum?
    var tableQRCode: ReservationsModel.Datum?
    var isQRCode: Bool = false
    var inovice: InvoiceModel.DataClass?
    var updateReservation: Bool = false
    var total: String?
    var coupon: CouponModel.DataClass?
    var paymentMethod: String?
    var updateFoodMenu: Bool = false
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        setup()
        fetchWallet()
        // Do any additional setup after loading the view.
    }
    
    func setup() {
        reserveIDConstraint.constant = -10
        bookModel = BookModel.instance()
        reserveDateLbl.text = bookModel?.date
        tableImage.setImage(url: table?.tableType?.pic)
        tableCodeLbl.text = "\("Table:".localized) \(table?.number?.string ?? "")"
        tablePrice.text = "\(table?.price ?? "") \("SAR".localized)"
        typeLbl.text = "\("Gathering type:".localized) \(table?.tableType?.name ?? "")"
        setupMenu()
        if isQRCode {
            setupQRCode()
        }
        
    }
    func setupPayment() {
        
        //walletLbl.text = "Pay".localized + " \(self.totalLbl.text ?? "") \("SAR".localized)"
        onlineView.borderColor = UIColor(red: 220/255, green: 222/255, blue: 225/255, alpha: 1)
        walletView.borderColor = UIColor(red: 220/255, green: 222/255, blue: 225/255, alpha: 1)
        
        let total = self.totalLbl.text?.cut(charSplit: " \("SAR".localized)", charWith: "").double() ?? 0
        
        ApiManager.instance.connection(api(.walletCheck, [total.int]), type: .get, completionHandler: { (response) in
            self.onlineView.UIViewAction {
                self.paymentMethod = "online"
                self.onlineView.borderColor = .appColor
                self.walletView.borderColor = UIColor(red: 220/255, green: 222/255, blue: 225/255, alpha: 1)
            }
            self.walletView.UIViewAction {
                self.paymentMethod = "wallet"
                self.walletView.borderColor = .appColor
                self.onlineView.borderColor = UIColor(red: 220/255, green: 222/255, blue: 225/255, alpha: 1)
            }
        }) { (response) in
            self.onlineView.UIViewAction {
                self.paymentMethod = "online"
                self.onlineView.borderColor = .appColor
                self.walletView.borderColor = UIColor(red: 220/255, green: 222/255, blue: 225/255, alpha: 1)
            }
            self.walletView.UIViewAction {
                self.makeWithoutCancelAlert("You do not have enough balance".localized, closure: {})
            }
        }
        
    }
    func setupQRCode() {
        couponTxf.isHidden = true
        walletView.isHidden = true
        onlineView.isHidden = true
        couponImage.isHidden = true
        haveCouponLbl.isHidden = true
        applyBtn.isHidden = true
        
        paymentHeight.constant = 0
        paymentMethodLbl.isHidden = true
        topPaymentConstaraint.constant = -45
        topReservationBill.constant = -40
        
        reserveDateLbl.text = tableQRCode?.date
        tableImage.setImage(url: tableQRCode?.table?.tableType?.image)
        tableCodeLbl.text = "\("Table:".localized) \(tableQRCode?.table?.number?.string ?? "")"
        tablePrice.text = "\(tableQRCode?.price ?? "") \("SAR".localized)"
        typeLbl.text = "\("Gathering type:") \(tableQRCode?.table?.tableType?.name ?? "")"
        
        fetchInovice()
    }
    func setupInvoice() {
      
        self.total = inovice?.resSubTotalPrice?.decimal(2)
        reserveDateLbl.text = "\(inovice?.resDate ?? "") \(inovice?.resStartTime ?? "")"
        initialValueLbl.text = "\(inovice?.resSubTotalPrice?.decimal(2) ?? "0") \("SAR".localized)"
        couponLbl.text = "-\(inovice?.resCouponPrice?.decimal(2) ?? "0") \("SAR".localized)"
        taxLbl.text = "+\(inovice?.resVatPrice?.decimal(2) ?? "0") \("SAR".localized)"
        totalLbl.text = "\(inovice?.resTotalPrice?.decimal(2) ?? "0") \("SAR".localized)"
        
        reserveIDLbl.text = inovice?.resID?.string
        reservationIDTitleLbl.isHidden = false
        reserveIDLbl.isHidden = false
        completeBtn.isHidden = true
        
        qrCodeImage.setImage(url: inovice?.qr)
        reserveIDConstraint.constant = 75
        
        //** setup update ** /
        if updateFoodMenu {
            completeBtn.isHidden = false
            productsTbl.delegate = self
            productsTbl.dataSource = self
            productsTbl.isHidden = false
            productsHeight.constant = 0
            productsTbl.reloadData()
            productsTbl.layoutIfNeeded()
            productsHeight.constant = (108) * (bookModel?.menu?.count ?? 0).cgFloat
            
            var discount = inovice?.resCouponPrice ?? 0
            self.total = tableQRCode?.price
            var total = self.total?.double() ?? 0
            for item in bookModel?.menu ?? [] {
                total += (item.price?.double() ?? 0) * (item.qty ?? 0).double
            }
            discount = ((total - (table?.price?.double() ?? 0)) / 100) * discount
            let tax = ((total - discount) / 100) * (BaseController.setting?.vat?.double() ?? 0)
            self.total = total.string
            initialValueLbl.text = "\(self.total ?? "0") \("SAR".localized)"
            couponLbl.text = "-\(discount) \("SAR".localized)"
            taxLbl.text = "+\(tax) \("SAR".localized)"
            totalLbl.text = "\((total+tax-discount).string) \("SAR".localized)"
            
        } else {
            if inovice?.products?.count ?? 0 > 0 {
                productsTbl.delegate = self
                productsTbl.dataSource = self
                productsTbl.isHidden = false
                productsHeight.constant = 0
                productsTbl.reloadData()
                productsTbl.layoutIfNeeded()
                productsHeight.constant = (108) * (inovice?.products?.count ?? 0).cgFloat
            }
        }
    }
    func fetchInovice() {
        startLoading()
        let method = api(.reservations, ["reservationsInvoice", tableQRCode?.id ?? 0, placeID ?? 0])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(InvoiceModel.self, from: response ?? Data())
            self.inovice = data?.data
            self.setupInvoice()
        }
    }
    func fetchCoupon() {
        startLoading()
        
        let method = api(.coupon, [placeID ?? 0, couponTxf.text ?? ""])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(CouponModel.self, from: response ?? Data())
            self.coupon = data?.data
            self.setup()
        }
    }
    func fetchWallet() {
        ApiManager.instance.connection(.wallet, type: .get) { (response) in
            let data = try? JSONDecoder().decode(WalletModel.self, from: response ?? Data())
            self.walletLbl.text = "Your balance is".localized + " \(data?.wallet ?? "0") \("SAR".localized)"
            self.setupPayment()
        }
    }
    func setupMenu() {
        if bookModel?.menu?.count ?? 0 > 0 {
            productsTbl.delegate = self
            productsTbl.dataSource = self
            productsTbl.isHidden = false
            productsHeight.constant = 0
            productsTbl.reloadData()
            productsTbl.layoutIfNeeded()
            productsHeight.constant = (108) * (bookModel?.menu?.count ?? 0).cgFloat
        
            var discount = coupon?.price?.double() ?? 0
            self.total = table?.price
            var total = self.total?.double() ?? 0
            for item in bookModel?.menu ?? [] {
                total += (item.price?.double() ?? 0) * (item.qty ?? 0).double
            }
            discount = ((total - (table?.price?.double() ?? 0)) / 100) * discount
            let tax = ((total - discount) / 100) * (BaseController.setting?.vat?.double() ?? 0)
            self.total = total.string
            initialValueLbl.text = "\(self.total ?? "0") \("SAR".localized)"
            couponLbl.text = "-\(discount) \("SAR".localized)"
            taxLbl.text = "+\(tax) \("SAR".localized)"
            totalLbl.text = "\((total+tax-discount).string) \("SAR".localized)"
            
        } else {
            total = table?.price
            let total = self.total?.double() ?? 0.0
            let tax = (total / 100) * (BaseController.setting?.vat?.double() ?? 0)
            productsTbl.isHidden = true
            productsHeight.constant = 0
            initialValueLbl.text = "\(table?.price ?? "0") \("SAR".localized)"
            couponLbl.text = "-0 \("SAR".localized)"
            taxLbl.text = "+\(tax) \("SAR".localized)"
            totalLbl.text = "\(total+tax) \("SAR".localized)"
            
        }
    }
   
    func setupParamters() {
        if bookModel?.menu?.count ?? 0 > 0 {

            var counter = 0
            for item in bookModel?.menu ?? [] {
                paramters["product_id[\(counter)]"] = item.id
                paramters["quantity[\(counter)]"] = item.qty
                paramters["price[\(counter)]"] = item.price
                counter += 1
            }
        }
    }
    func setupParamtersReturen() -> [String: Any] {
        let bookModel = BookModel.instance()
        var paramters: [String: Any] = [:]
        if bookModel.menu?.count ?? 0 > 0 {
            var counter = 0
            for item in bookModel.menu ?? [] {
                paramters["product_id[\(counter)]"] = item.id
                paramters["quantity[\(counter)]"] = item.qty
                paramters["price[\(counter)]"] = item.price
                counter += 1
            }
        }
        return paramters
    }
    @IBAction func applyCoupon(_ sender: Any) {
        if couponTxf.text?.count == 0 {
            return makeWithoutCancelAlert("Please enter coupon".localized, closure: {})
        }
        fetchCoupon()
    }
    @IBAction func complete(_ sender: Any) {
        if updateFoodMenu {
            startLoading()
            var paramters: [String: Any] = [:]
            paramters = setupParamtersReturen()
            paramters["res_id"] = tableQRCode?.id
            let method = api(.reservations, [placeID ?? 0, "add"])
            ApiManager.instance.paramaters = paramters
            ApiManager.instance.connection(method, type: .post) { (response) in
                BookModel.reset()
                let scene = self.controller(OrderCompletedController.self, storyboard: .reservation)
                scene.reservtion = self.tableQRCode
                self.push(scene)
            }
            return
        }
        if paymentMethod == nil {
            return makeWithoutCancelAlert("Please select payment method".localized, closure: {})
        }
        if isQRCode {
            return
        }
        
        if paymentMethod == "wallet" {
            createReservation()
        } else {
            goSellSDK()
        }
    }
    
    func createReservation(paymentID: String? = nil) {
        startLoading()
        setupParamters()
        paramters["notes"] = ""
        if paymentID != nil {
            paramters["paid"] = "1"
        } else {
            paramters["paid"] = "0"
        }
        paramters["coupon"] = coupon?.price ?? "0"
        paramters["payment"] = paymentMethod ?? "online"
        let method = api(.reservations, [placeID ?? 0, "add"])
        ApiManager.instance.paramaters = paramters
        ApiManager.instance.connection(method, type: .post) { (response) in
            BookModel.reset()
            let data = try? JSONDecoder().decode(OrderCompleteModel.self, from: response ?? Data())
            //            let scene = self.controller(OrderCompletedController.self, storyboard: .reservation)
            //            scene.order = data?.data
            //            self.push(scene)
            
            let scene = self.controller(ReservationDetailController.self, storyboard: .reservation)
            scene.reservationID = data?.data?.resID
            scene.isPrev = false
            self.push(scene)
        }
    }
    
}
extension BillDetailController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if inovice?.products?.count ?? 0 > 0 && !updateFoodMenu {
            return inovice?.products?.count ?? 0
        }
        return bookModel?.menu?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.cell(type: MenuItemQuantity.self, indexPath)
        if (inovice?.products?.count ?? 0 > 0) && !updateFoodMenu {
            let model = inovice?.products?[indexPath.row]
            cell.itemImage.setImage(url: model?.image)
            cell.itemNameLbl.text = model?.name
            cell.itemDescLbl.text = model?.details
            cell.priceLbl.text = "\(((model?.price?.double() ?? 0) * (model?.quantity ?? 0).double).decimal(1)) \("SAR".localized)"
            cell.qty = model?.quantity ?? 0
        } else {
            let model = bookModel?.menu?[indexPath.row]
            cell.itemImage.setImage(url: model?.pic)
            cell.itemNameLbl.text = model?.name
            cell.itemDescLbl.text = model?.details
            cell.priceLbl.text = "\(((model?.price?.double() ?? 0) * (model?.qty ?? 0).double).decimal(1)) \("SAR".localized)"
            cell.menu = model
            cell.qty = model?.qty ?? 0
        }
        
        cell.setup()
        cell.shppingView.isHidden = true
        cell.disable = true
        return cell
    }
}

extension BillDetailController: goSellSDK.SessionDelegate, SessionDataSource {
    func goSellSDK() {
        GoSellSDK.mode = .production
        let secretKey = SecretKey(sandbox: "sk_test_1hoP0u7mAXGpsJ9eRa6tylnx", production: "sk_live_pzKeVGhmdfN5MTgQWwtX19aD")
        GoSellSDK.secretKey = secretKey
        let session = Session()
        session.delegate = self
        session.dataSource = self
        session.start()
        print(session.canStart)
    }
    var customer: Customer? {
        let emailAddress = try! EmailAddress(emailAddressString: UserRoot.fetch()?.data?.email ?? "Email@email.com")
        let phoneNumber = try! PhoneNumber(isdNumber: "966", phoneNumber: "548612347")
        
        return try? Customer(emailAddress:  emailAddress,
                             phoneNumber:   phoneNumber,
                             firstName:     UserRoot.fetch()?.data?.name ?? "Name",
                             middleName:    nil,
                             lastName:      UserRoot.fetch()?.data?.name ?? "Name")
    }
    var currency: Currency? {
        
        return .with(isoCode: "SAR")
    }
    var amount: Decimal {
        let string = self.totalLbl.text?.cut(charSplit: " \("SAR".localized)", charWith: "")
        return Decimal(string?.double() ?? 0)
    }
    internal var mode: TransactionMode {
        return .purchase
    }
    var items: [PaymentItem]? {
        var array: [PaymentItem] = []
        //        for item in CartDefaults.getCart() {
        //            let oneUnit = Quantity(value: Decimal(item.customersBasketQuantity ?? 0), unitOfMeasurement: .units)
        //            let firstItem = PaymentItem(title:          item.productsName ?? "",
        //                                        quantity:       oneUnit,
        //                                        amountPerUnit:  Decimal(item.price?.double() ?? 0))
        //            array.append(firstItem)
        //        }
        let string = self.totalLbl.text?.cut(charSplit: " \("SAR".localized)", charWith: "")

        let oneUnit = Quantity(value: Decimal(1), unitOfMeasurement: .units)
        let firstItem = PaymentItem(title:          "Item #1",
                                    quantity:       oneUnit,
                                    amountPerUnit:  Decimal(string?.double() ?? 0))
        array.append(firstItem)
        return array
    }
    func paymentSucceed(_ charge: Charge, on session: SessionProtocol) {
        createReservation(paymentID: charge.identifier)
    }
    func paymentFailed(with charge: Charge?, error: TapSDKError?, on session: SessionProtocol) {
        
    }
    func sessionIsStarting(_ session: SessionProtocol) {
        
    }
    func sessionHasStarted(_ session: SessionProtocol) {
        
    }
    func sessionHasFailedToStart(_ session: SessionProtocol) {
        
    }
    func sessionCancelled(_ session: SessionProtocol) {
        
    }
}
