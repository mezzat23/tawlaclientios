//
//  Ordersucces.swift
//  SupportI
//
//  Created by Adam on 3/24/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class OrderCompletedController: BaseController {
    @IBOutlet weak var reservationIDLbl: UILabel!
    @IBOutlet weak var reservationDateLbl: UILabel!
    @IBOutlet weak var invoiceLbl: UILabel!
    @IBOutlet weak var paymentMethodLbl: UILabel!
    
    var order: OrderCompleteModel.DataClass?
    var reservtion: ReservationsModel.Datum?
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }

    func setup() {
        reservationIDLbl.text = reservtion?.id?.string
        reservationDateLbl.text = reservtion?.date
        invoiceLbl.text = reservtion?.price
        paymentMethodLbl.text = ""
    }
    @IBAction func home(_ sender: Any) {
        let scene = UIStoryboard.init(name: "Main", bundle: nil).instantiateInitialViewController()
        let delegate = UIApplication.shared.delegate as? AppDelegate
        delegate?.window?.rootViewController = scene
    }
}
