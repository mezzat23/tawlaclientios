//
//  Singleresturant.swift
//  SupportI
//
//  Created by Adam on 3/26/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class TableQRScanController: BaseController {
    @IBOutlet weak var resturantLbl: UILabel!
    @IBOutlet weak var resImage: UIImageView!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var paymentLbl: UILabel!
    @IBOutlet weak var waiterView: UIView!
    @IBOutlet weak var foodMenuView: UIView!
    @IBOutlet weak var requestBillView: UIView!
    
    var reservation: ReservationsModel.Datum?
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
    
    func setup() {
        resturantLbl.text = reservation?.place?.name
        resImage.setImage(url: reservation?.place?.image)
        descLbl.text = "\("Table: ".localized) \(reservation?.table?.number ?? 0)"
        paymentLbl.text = reservation?.place?.payment_types
        
        //var paramters: [String: Any] = [:]
        
    }

    @IBAction func foodMenu(_ sender: Any) {
        
        var book = BookModel.instance()
        if reservation?.table?.hasBarrier ?? 0 == 1 {
            book.isCover = true
        } else {
            book.isCover = false
        }
        book.persons = self.reservation?.adultCount
        book.childs = self.reservation?.childrenCount
        book.typeID = self.reservation?.table?.tableType?.id
        book.position = self.reservation?.table?.position
        book.date = self.reservation?.date
        book.QRCODE = true
        book.resturantID = self.reservation?.place?.id
        book.save()
        
        let scene = self.controller(ResturantMenuController.self, storyboard: .resturant)
        scene.fromOrder = true
        //scene.paramtersOrder = paramters
        scene.resturantID = self.reservation?.place?.id
        scene.tableQRCode = self.reservation
        scene.isQRCode = true
        self.push(scene)
    }
    @IBAction func orderWaiter(_ sender: Any) {
        startLoading()
        let method = api(.reservations, ["callWaiter", reservation?.id ?? 0, reservation?.place?.id ?? 0])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            self.makeWithoutCancelAlert("The restaurant has been successfully notified to you. Please wait a bit, thank you".localized, closure: {})
        }
    }
    @IBAction func requestBill(_ sender: Any) {
        let scene = self.controller(BillDetailController.self, storyboard: .book)
        //scene.paramters = paramters
        scene.placeID = self.reservation?.place?.id
        scene.tableQRCode = self.reservation
        scene.isQRCode = true
        self.push(scene)
    }
    @IBAction func backToMain(_ sender: Any) {
        self.navigationController?.pop()
    }
}
