//
//  TableAccount.swift
//  SupportI
//
//  Created by Adam on 3/31/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class CompeleteReservationController: BaseController {
    enum TableType {
        case free
        case vip
    }
    @IBOutlet weak var positionLbl: UILabel!
    @IBOutlet weak var freeBtn: RadioButton!
    @IBOutlet weak var vipBtn: RadioButton!
    @IBOutlet weak var tablesCollection: UICollectionView! {
        didSet {
            tablesCollection.delegate = self
            tablesCollection.dataSource = self
        }
    }
    
    var tables: [TablesModel.Datum] = []
    var placeID: Int?
    var bookModel: BookModel?
    var paramters: [String: Any] = [:]
    var tableType: TableType?
    var tableSelected: Int?
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        handlers()
        setup()
        // Do any additional setup after loading the view.
    }
    func setup() {
        bookModel = BookModel.instance()
        positionLbl.text = bookModel?.position?.localized.limit
        if bookModel?.position == BookModel.Position.outDoor.rawValue {
            paramters["position"] = "outside"
        } else {
            paramters["position"] = "inside"
        }
        if tableType == .vip {
            paramters["price"] = "vip"
        } else if tableType == .free {
            paramters["price"] = "free"
        } else {
            paramters["price"] = "all"
        }
        paramters["type"] = "all"
        if case bookModel?.isCover = true {
            paramters["has_barrier"] = "1"

        } else {
            paramters["has_barrier"] = "all"

        }
        paramters["adult_count"] = bookModel?.persons ?? 0
        paramters["children_count"] = bookModel?.childs ?? 0
        fetchTables()
    }
    func reload() {
        tablesCollection.reloadData()
    }
    func handlers() {
        vipBtn.onSelect {
            self.tableType = .vip
            self.freeBtn.deselect()
            self.setup()
        }
        vipBtn.onDeselect {
            if self.tableType == .vip {
                self.tableType = nil
                self.setup()
            }
        }
        freeBtn.onSelect {
            self.tableType = .free
            self.vipBtn.deselect()
            self.setup()
        }
        freeBtn.onDeselect {
            if self.tableType == .free {
                self.tableType = nil
                self.setup()
            }
        }
    }
    func showAlertMenu(paramters: [String: Any]) {
        let alert = UIAlertController(title: "alert.lan".localized, message: "Do you want to add from the menu to your order?".localized, preferredStyle: UIAlertController.Style.alert)
        let acceptAction = UIAlertAction(title: "Yes".localized, style: .default) { (_) -> Void in
            let scene = self.controller(ResturantMenuController.self, storyboard: .resturant)
            scene.fromOrder = true
            scene.paramtersOrder = paramters
            scene.resturantID = self.placeID
            scene.table = self.tables[safe: self.tableSelected ?? 0]
            self.push(scene)
        }
        let cancelAction = UIAlertAction(title: "No".localized, style: .default) { (_) -> Void in
            let scene = self.controller(BillDetailController.self, storyboard: .book)
            scene.paramters = paramters
            scene.placeID = self.placeID
            scene.table = self.tables[safe: self.tableSelected ?? 0]
            self.push(scene)
        }
        alert.addAction(acceptAction)
        alert.addAction(cancelAction)
        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
       
    }
    @IBAction func positionChange(_ sender: Any) {
        let scene = self.controller(PickerViewHelper.self, storyboard: .PickerViewHelper)
        scene.source = [BookModel.Position.inDoor.rawValue, BookModel.Position.outDoor.rawValue]
        scene.titleClosure = { row in
            if row == 0 {
                return BookModel.Position.inDoor.rawValue.localized
            } else {
                return BookModel.Position.outDoor.rawValue.localized
            }
        }
        scene.didSelectClosure = { row in
            if row == 0 {
                self.bookModel?.position = BookModel.Position.inDoor.rawValue
            } else {
                self.bookModel?.position = BookModel.Position.outDoor.rawValue
            }
            self.bookModel?.save()
            self.setup()
        }
        self.pushPop(vcr: scene)
    }
    @IBAction func complete(_ sender: Any) {
        if UserRoot.token() == nil {
            let scene = controller(LoginController.self, storyboard: .pop)
            scene.delegate = self
            pushPop(vcr: scene)
            return
        } else if UserRoot.fetch()?.data?.verified == 0 {
            let scene = controller(LoginController.self, storyboard: .pop)
            scene.delegate = self
            pushPop(vcr: scene)
            return
        }
        
        if tableSelected == nil {
            makeAlert("You must select the table will reserved it".localized, closure: {})
            return
        }
        var paramters: [String: Any] = [:]
        let startTime = DateHelper().date(date: bookModel?.date, format: "HH:mm", oldFormat: "yyyy-MM-dd HH:mm")
        let date = DateHelper().date(date: bookModel?.date, format: "yyyy-MM-dd", oldFormat: "yyyy-MM-dd HH:mm")
        if bookModel?.position == BookModel.Position.outDoor.rawValue {
            paramters["position"] = "outside"
        } else {
            paramters["position"] = "inside"
        }
        if tableType == .vip {
            paramters["price"] = "vip"
        } else if tableType == .free {
            paramters["price"] = "free"
        } else {
            paramters["price"] = "all"
        }
        paramters["type"] = "3"
        if case bookModel?.isCover = true {
            paramters["has_barrier"] = "1"
            
        } else {
            paramters["has_barrier"] = "0"
            
        }
        paramters["adult_count"] = bookModel?.persons ?? 0
        paramters["children_count"] = bookModel?.childs ?? 0
        paramters["table_id"] = tables[safe: tableSelected ?? 0]?.id ?? 0
        paramters["notes"] = "Note"
        if case bookModel?.QRCODE = true {
            paramters["date"] = nil
            paramters["start_at"] = nil
            paramters["end_at"] = nil
            paramters["mechanism"] = "queue"
        } else {
            paramters["date"] = date
            paramters["start_at"] = startTime
            //paramters["end_at"] = startTime
            paramters["mechanism"] = "time"
        }
        
        paramters["gathering_type_id"] = bookModel?.typeID ?? 0

        
        self.showAlertMenu(paramters: paramters)
        
    }
}
// MARK:  Network
extension CompeleteReservationController {
    func fetchTables() {
        startLoading()
        let method = api(.places, [placeID ?? 0, "tables"])
        ApiManager.instance.paramaters = paramters
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(TablesModel.self, from: response ?? Data())
            self.tables.removeAll()
            self.tables.append(contentsOf: data?.data ?? [])
            self.reload()
        }
    }
}
extension CompeleteReservationController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 110)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tables.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = collectionView.cell(type: TableCollectionCell.self, indexPath)
        if tableSelected == indexPath.row {
            cell.checkImage.isHidden = false
        } else {
            cell.checkImage.isHidden = true
        }
        cell.model = tables[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        tableSelected = indexPath.row
        collectionView.reloadData()
    }
}
// MARK:  Login delegate
extension CompeleteReservationController: LoginDelegate, RegisterDelegate {
    func didOpenRegister() {
        let scene = controller(RegisterController.self, storyboard: .pop)
        scene.delegate = self
        pushPop(vcr: scene)
    }
    func didLogin() {
        if UserRoot.fetch()?.data?.verified == 0 {
            let scene = controller(VerfiycodeController.self, storyboard: .pop)
            scene.delegate = self
            scene.prev = true
            scene.closurePrevVC = {
                let scene = self.controller(LoginController.self, storyboard: .pop)
                scene.delegate = self
                self.pushPop(vcr: scene)
            }
            pushPop(vcr: scene)
        }
    }
    func didVerify() {
        
    }
    func didRegister() {
        if UserRoot.fetch()?.data?.verified == 0 {
            let scene = controller(VerfiycodeController.self, storyboard: .pop)
            scene.delegate = self
            scene.prev = true
            scene.closurePrevVC = {
                let scene = self.controller(RegisterController.self, storyboard: .pop)
                scene.delegate = self
                self.pushPop(vcr: scene)
            }
            pushPop(vcr: scene)
        }
    }
}
