//
//  Reservationrating.swift
//  SupportI
//
//  Created by Adam on 3/30/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class RateReservationController: BaseController {
    @IBOutlet weak var resturantImage: UIImageView!
    @IBOutlet weak var resturantNameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var typeCountLbl: UILabel!
    @IBOutlet weak var tableNumLbl: UILabel!
    @IBOutlet weak var ratesTbl: UITableView!
    @IBOutlet weak var ratesHeight: NSLayoutConstraint!
    @IBOutlet weak var commentTxf: UITextField!
    @IBOutlet weak var detailsTxf: UITextField!
    
    var reservation: ReservationsModel.Datum?
    var closureDetailReservation: HandlerView?
    var paramters: [String: Any] = [:]
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        setup()
    }
    func setup() {
        commentTxf.addPadding(10)
        detailsTxf.addPadding(10)
        ratesTbl.delegate = self
        ratesTbl.dataSource = self
        resturantImage.setImage(url: reservation?.place?.image)
        resturantNameLbl.text = reservation?.place?.name
        dateLbl.text = reservation?.date
        typeLbl.text = reservation?.gatheringType?.name
        typeCountLbl.text = "\((reservation?.adultCount ?? 0) + (reservation?.childrenCount ?? 0))"
        tableNumLbl.text = reservation?.table?.id?.string ?? "0"
        ratesTbl.reloadData()
        ratesHeight.constant = ratesTbl.contentSize.height
    }
    
    @IBAction func rate(_ sender: Any) {
        if paramters["service_rate"] == nil || paramters["place_rate"] == nil || paramters["food_rate"] == nil {
            makeAlert("Please rate the place !".localized, closure: {})
            return
        }
        if case commentTxf.text?.isEmpty = true {
            makeAlert("Please write your comment !".localized, closure: {})
            return
        }
        startLoading()
        let method = api(.reservations, [reservation?.id ?? 0, "rates"])
        ApiManager.instance.paramaters = paramters
        ApiManager.instance.connection(method, type: .post) { (response) in
            self.makeWithoutCancelAlert("Your rate is has been send successfully".localized, closure: {
                self.backBtn(self)
                self.closureDetailReservation?()
            })
        }
    }
}

extension RateReservationController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.cell(type: Makerate.self, indexPath)
        cell.delegate = self
        cell.rateView.rating = 0
        if indexPath.row == 0 {
            cell.rateLbl.text = "Commitment to reservation".localized
        } else if indexPath.row == 1 {
            cell.rateLbl.text = "Cleanliness of the place".localized
        } else {
            cell.rateLbl.text = "Eating quality".localized
        }
        cell.setup()
        return cell
    }
}
extension RateReservationController: MakeRateDelegate {
    func didChangeRate(path: Int, rate: Double) {
        if path == 0 {
            paramters["service_rate"] = rate
        } else if path == 1 {
            paramters["place_rate"] = rate
        } else {
            paramters["food_rate"] = rate
        }
    }
}
