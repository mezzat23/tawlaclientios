//
//  Wallet.swift
//  SupportI
//
//  Created by Adam on 3/24/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class Wallet: BaseController {
    @IBOutlet weak var currentBalanceLbl: UILabel!
    @IBOutlet weak var transLbl: UILabel!
    @IBOutlet weak var walletEmpty: UIImageView!
    @IBOutlet weak var walletTbl: UITableView! {
        didSet {
            walletTbl.delegate = self
            walletTbl.dataSource = self
        }
    }

    var wallet: [WalletModel.Datum] = []
    var walletData: String?
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        setup()
        fetchWallet()
    }
    func setup() {
        currentBalanceLbl.text = walletData ?? "0.00 \("SAR".localized)"
        if wallet.count > 0 {
            walletEmpty.isHidden = true
            walletTbl.isHidden = false
            transLbl.isHidden = false
            walletTbl.reloadData()
        } else {
            walletEmpty.isHidden = false
            walletTbl.isHidden = true
            transLbl.isHidden = true
        }
    }
    func fetchWallet() {
        startLoading()
        ApiManager.instance.connection(.wallet, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(WalletModel.self, from: response ?? Data())
            self.walletData = "\(data?.wallet?.double()?.decimal(2) ?? "0.00") \("SAR".localized)"
            self.wallet.append(contentsOf: data?.data?.data ?? [])
            self.setup()
        }
    }
    @IBAction func checkCoupon(_ sender: Any) {
        
    }
}

// MARK:  Table View
extension Wallet: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wallet.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: Walletviewcell.self, indexPath)
        cell.model = wallet[indexPath.row]
        return cell
    }
    
}
