//
//  Notifications.swift
//  SupportI
//
//  Created by Adam on 3/26/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class NotificationsController: BaseController {
    @IBOutlet weak var notificationsTbl: UITableView! {
        didSet {
            notificationsTbl.delegate = self
            notificationsTbl.dataSource = self
        }
    }
    @IBOutlet weak var notiImage: UIImageView!
    @IBOutlet weak var notiLbl1: UILabel!
    @IBOutlet weak var notiLbl2: UILabel!
    
    var notifications: [NotificationModel.Datum] = []
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        notiImage.isHidden = true
        notiLbl1.isHidden = true
        notiLbl2.isHidden = true
        notificationsTbl.isHidden = false
        fetchNotifications()
    }
    func setup() {
        if notifications.count > 0 {
            notiImage.isHidden = true
            notiLbl1.isHidden = true
            notiLbl2.isHidden = true
            notificationsTbl.isHidden = false
            notificationsTbl.reloadData()
        } else {
            notiImage.isHidden = false
            notiLbl1.isHidden = false
            notiLbl2.isHidden = false
            notificationsTbl.isHidden = true
        }
    }

}
// MARK:  Network
extension NotificationsController {
    func fetchNotifications() {
        startLoading()
        ApiManager.instance.connection(.notifications, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(NotificationModel.self, from: response ?? Data())
            if data?.data?.count ?? 0 > 0 {
                self.notifications.append(contentsOf: data?.data ?? [])
            }
            ViewModelCore().paginator(respnod: data?.data)
            self.setup()
        }
    }
}
// MARK:  Table view
extension NotificationsController: UITableViewDelegate, UITableViewDataSource {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        notificationsTbl.swipeButtomRefresh {
            if ViewModelCore().runPaginator() {
                self.fetchNotifications()
            } else {
                self.notificationsTbl.stopSwipeButtom()
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: NotificationCell.self, indexPath)
        cell.model = notifications[indexPath.row]
        return cell
    }
}
