//
//  Favoraite.swift
//  SupportI
//
//  Created by Adam on 3/19/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class FavoraiteController: BaseController {
    @IBOutlet weak var favoriteTbl: UITableView! {
        didSet {
            favoriteTbl.delegate = self
            favoriteTbl.dataSource = self
        }
    }
    var resturants: [AllResturantModel.Datum] = []
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        setup()
        fetchFavorites()
        // Do any additional setup after loading the view.
    }
    func setup() {
        
    }
}
// MARK:  Network
extension FavoraiteController {
    func fetchFavorites() {
        startLoading()
        ApiManager.instance.connection(.favorites, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(AllResturantModel.self, from: response ?? Data())
            self.resturants.append(contentsOf: data?.data ?? [])
            self.favoriteTbl.reloadData()
        }
    }
}
// MARK:  Table View
extension FavoraiteController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resturants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: Resturants.self, indexPath)
        cell.model = resturants[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let scene = controller(ResturantDetailsController.self, storyboard: .resturant)
        scene.resturantID = resturants[indexPath.row].id
        self.push(scene)
    }
}
