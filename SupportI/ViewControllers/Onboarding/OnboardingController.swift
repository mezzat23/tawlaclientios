//
//  Ordercontinue.swift
//  SupportI
//
//  Created by Adam on 3/30/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class OnboardingController: BaseController {
    @IBOutlet weak var imagesCollection: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var letsGoBtn: UIButton!
    
    static var ONBOARDING = "ONBOARDING"
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
    func setup() {
        if UserDefaults.standard.bool(forKey: OnboardingController.ONBOARDING) {
            guard let scene = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() else { return }
            let appDelegate = UIApplication.shared.delegate
            appDelegate?.window??.rootViewController = scene
            return
        }
        imagesCollection.delegate = self
        imagesCollection.dataSource = self
        imagesCollection.reloadData()
    }

    @IBAction func letsStart(_ sender: Any) {
        if pageControl.currentPage == 2 {
            UserDefaults.standard.set(true, forKey: OnboardingController.ONBOARDING)
            guard let scene = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() else { return }
            push(scene)
        } else {
            imagesCollection.moveToNextItem()
        }
    }
}
extension OnboardingController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width, height: collectionView.height)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageControl.currentPage = indexPath.row
        if indexPath.row == 2 {
            letsGoBtn.setTitle("Let's start".localized, for: .normal)
        } else {
            letsGoBtn.setTitle("Next".localized, for: .normal)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControl.numberOfPages = 3
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.cell(type: OnboardingCell.self, indexPath)
        return setupCell(cell: cell)
    }
    
    func setupCell(cell: OnboardingCell) -> OnboardingCell {
        switch cell.indexPath() {
            case 0:
                cell.imageView.image = UIImage(named: "illus 01")
                cell.titleLbl.text = "Easily schedule your reservations".localized
                cell.descLbl.text = "This text is an example of text that can be replaced in the same space. It has been generated".localized
                cell.descLbl.text = ""
            case 1:
                cell.imageView.image = UIImage(named: "illus 02")
                cell.titleLbl.text = "Get instant discounts when booking".localized
                cell.descLbl.text = "This text is an example of text that can be replaced in the same space. It has been generated".localized
                cell.descLbl.text = ""
            case 2:
                cell.imageView.image = UIImage(named: "illus 03")
                cell.titleLbl.text = "Find the closest suitable places".localized
                cell.descLbl.text = "This text is an example of text that can be replaced in the same space. It has been generated".localized
                cell.descLbl.text = ""
            default:
            break
        }
        return cell
    }
}
